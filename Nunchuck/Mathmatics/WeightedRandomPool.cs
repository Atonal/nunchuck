﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Nunchuck.Mathmatics
{
    public class WeightedRandomPool<T>
    {
        public void Add(T element, double weight)
        {
            Debug.Assert(weight > 0);
            list.Add(element);
            totalWeight += weight;
            partialSums.Add(totalWeight);
        }

        public void Clear()
        {
            list.Clear();
            partialSums.Clear();
            totalWeight = 0;
        }

        public bool IsEmpty()
        {
            return list.Count == 0;
        }

        public T GetRandom(Random rand)
        {
            Debug.Assert(list.Count > 0);
            double w = rand.NextDouble() * totalWeight;
            for (int i = 0; i < partialSums.Count; i++)
            {
                if (w < partialSums[i])
                {
                    return list[i];
                }
            }

            return list[list.Count - 1];
        }

        List<T> list = new List<T>();
        List<double> partialSums = new List<double>();
        double totalWeight = 0;
    }
}
