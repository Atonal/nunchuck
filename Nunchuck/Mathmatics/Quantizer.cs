﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Nunchuck.Mathmatics
{
    public class Quantizer
    {
        public void AddValue(float value)
        {
            values.Add(value, value);
        }

        public float Quntize(float value)
        {
            Debug.Assert(values.Count > 0, "먼저 양자화 할 값을 지정해야 합니다.");
            float minDist = float.MaxValue;
            float ret = float.NaN;
            foreach (var v in values.Values)
            {
                float dist = Math.Abs(v - value);
                if (dist < minDist)
                {
                    minDist = dist;
                    ret = v;
                }
            }
            return ret;
        }

        SortedList<float, float> values = new SortedList<float, float>();
    }
}
