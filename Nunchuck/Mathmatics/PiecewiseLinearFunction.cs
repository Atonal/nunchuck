﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Nunchuck.Mathmatics
{
    public class PiecewiseLinearFunction
    {
        public void AddControlPoint(float x, float y)
        {
            if (controlPoints.ContainsKey(x))
                throw new ArgumentOutOfRangeException("하나의 x값에 여러 y값을 지정할 수 없습니다.");
            controlPoints.Add(x, y);
        }

        public float GetValueAt(float x)
        {
            Debug.Assert(controlPoints.Count > 0, "값을 얻기전에 특징점들을 먼저 입력해야 합니다.");
            Debug.Assert(controlPoints.First().Key <= controlPoints.Last().Key);

            if (x <= controlPoints.First().Key)
                return controlPoints.First().Value;
            if (x >= controlPoints.Last().Key)
                return controlPoints.Last().Value;

            for (int i = 1; i < controlPoints.Count; i++)
            {
                var prev = controlPoints.ElementAt(i - 1);
                var next = controlPoints.ElementAt(i);
                if (prev.Key <= x && x <= next.Key)
                {
                    Debug.Assert(prev.Key < next.Key);
                    var nextRatio = (x - prev.Key) / (next.Key - prev.Key);
                    Debug.Assert(0 <= nextRatio && nextRatio <= 1);
                    return prev.Value * (1 - nextRatio) + next.Value * nextRatio;
                }
            }

            Debug.Assert(false);
            return 0;
        }

        SortedList<float, float> controlPoints = new SortedList<float, float>();
    }
}
