﻿
namespace Nunchuck
{
    public struct Second
    {
        private float value;
        public static Second Zero = new Second(0);
        public static Second Infinity = new Second(float.PositiveInfinity);

        #region .ctor
        private Second(float value)
        {
            this.value = value;
        }
        #endregion

        #region casting
        public static explicit operator Second(float time)
        {
            return new Second(time);
        }
        public static implicit operator float(Second time)
        {
            return time.value;
        }
        #endregion
        #region operator
        public static Second operator -(Second lhs, Second rhs)
        {
            return (Second)(lhs.value - rhs.value);
        }

        public static Second operator -(float lhs, Second rhs)
        {
            return (Second)(lhs - rhs.value);
        }

        public static Second operator -(Second lhs, float rhs)
        {
            return (Second)(lhs.value - rhs);
        }

        public static Second operator +(Second lhs, Second rhs)
        {
            return (Second)(lhs.value + rhs.value);
        }

        public static Second operator +(Second lhs, float rhs)
        {
            return (Second)(lhs.value + rhs);
        }

        public static Second operator +(float lhs, Second rhs)
        {
            return (Second)(lhs + rhs.value);
        }

        public static Second operator *(Second lhs, Second rhs)
        {
            return (Second)(lhs.value * rhs.value);
        }

        public static Second operator *(Second lhs, float rhs)
        {
            return (Second)(lhs.value * rhs);
        }

        public static Second operator *(float lhs, Second rhs)
        {
            return (Second)(lhs * rhs.value);
        }

        public static Second operator /(Second lhs, Second rhs)
        {
            return (Second)(lhs.value / rhs.value);
        }

        public static Second operator /(Second lhs, float rhs)
        {
            return (Second)(lhs.value / rhs);
        }

        public static Second operator /(float lhs, Second rhs)
        {
            return (Second)(lhs / rhs.value);
        }

        #endregion

        #region override
        public override string ToString()
        {
            return value + "seconds";
        }
        #endregion
    }
}
