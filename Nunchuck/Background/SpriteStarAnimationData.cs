﻿
namespace Nunchuck.Background
{
    public struct SpriteStarAnimationData
    {
        public Radian Rotation;
        public float Scale;
    }
}
