﻿using OpenTK;
using OpenTK.Graphics;
using System.Drawing;

namespace Nunchuck.Background
{
    public class StarColor
    {
        public static readonly Color4 DarkRed = Color.FromArgb(255, 64, 0, 0);
        public static readonly Color4 Red = Color.FromArgb(255, 240, 0, 0);
        public static readonly Color4 Orange = Color.FromArgb(255, 255, 138, 56);
        public static readonly Color4 BrightOrange = Color.FromArgb(255, 255, 233, 203);
        public static readonly Color4 White = Color.FromArgb(255, 255, 246, 233);
        public static readonly Color4 BrightBlue = Color.FromArgb(255, 248, 249, 255);
        public static readonly Color4 SkyBlue = Color.FromArgb(255, 177, 204, 255);
        public static readonly Color4 Blue = Color.FromArgb(255, 68, 114, 255);
        public static readonly Color4 DarkBlue = Color.FromArgb(255, 2, 71, 254);
    }
}
