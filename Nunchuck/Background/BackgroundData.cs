﻿
namespace Nunchuck.Background
{
    public struct BackgroundData
    {
        public SimpleStarData[] DotStars;
        public SimpleStarData[] QuadStars;
        public NebularData[] Nebulas;
        public SpriteStarData[] SpriteStars;
        public DoodadData[] Doodads;
    }
}
