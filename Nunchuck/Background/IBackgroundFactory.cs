﻿
namespace Nunchuck.Background
{
    public interface IBackgroundFactory
    {
        BackgroundData Create();
    }
}
