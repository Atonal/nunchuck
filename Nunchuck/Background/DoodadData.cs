﻿using OpenTK;

namespace Nunchuck.Background
{
    public struct DoodadData
    {
        public Vector3 Position;
        public float Scale;
        public Radian Rotation;
        public string Texture;
        public DoodadLayer Layer;
    }
}
