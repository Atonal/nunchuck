﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;

namespace Nunchuck.Background
{
    public struct ColorPalette
    {
        public Color4 ColorFrom;
        public Color4 ColorTo;
        public FloatRange ToBlend;    // 0에서 1사이
        public FloatRange Brightness;   // 0에서 1사이

        public Vector3 GetColor3(Random rand)
        {
            float toRatio = rand.NextFloat(ToBlend.Min, ToBlend.Max);
            var color = ColorHelper.Lerp((Color)ColorFrom, (Color)ColorTo, toRatio);
            return color.ToVector3();
        }

        public float GetBrightness(Random rand)
        {
            return rand.NextFloat(Brightness.Min, Brightness.Max);
        }

        public Color4 GetColor4(Random rand)
        {
            var toRatio = rand.NextFloat(ToBlend.Min, ToBlend.Max);
            var color = ColorHelper.Lerp((Color)ColorFrom, (Color)ColorTo, toRatio);
            var brightness = GetBrightness(rand);
            color = ColorHelper.Multiply(color, brightness);
            return new Color4(color.R, color.G, color.B, 255);
        }
    }
}
