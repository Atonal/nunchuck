﻿using Nunchuck.Rendering;
using OpenTK;
using OpenTK.Graphics;
using System;

namespace Nunchuck.Background
{
    public class SpriteStarRenderer : IDisposable
    {
        public const int MaxAnimationGroup = 8;

        public SpriteStarRenderer(TextureCache textureCache, SpriteStarData[] data)
        {
            animations = new SpriteStarAnimation[MaxAnimationGroup];
            groupData = new SpriteStarAnimationData[MaxAnimationGroup];
            for (int i = 0; i < MaxAnimationGroup; i++)
            {
                animations[i] = new SpriteStarAnimation(this, i);
                groupData[i] = new SpriteStarAnimationData()
                {
                    Scale = 1.0f,
                    Rotation = (Radian)0.0f
                };
            }

            lightSourceTexture = textureCache.GetTexture("Resource/Background/LightSource.png");
            flareTexture = textureCache.GetTexture("Resource/Background/Flare.png");

            sourceBillboards = new BillboardBuilder(data.Length);
            flareBillboards = new BillboardBuilder(data.Length, BillboardBuilderOption.NoElementBuffer);
            elementBuffer = ElementBuffer.Create(sourceBillboards.Elements);
            sourceVertexBuffer = VertexBuffer.CreateDynamic(sourceBillboards.Vertices, ShaderProvider.SimpleTexture);
            flareVertexBuffer = VertexBuffer.CreateDynamic(flareBillboards.Vertices, ShaderProvider.SimpleTexture);

            sourceData = data;
        }

        public SpriteStarAnimation GetAnimation(int animationGroup)
        {
            if (animationGroup < 0 || animationGroup >= MaxAnimationGroup)
                throw new ArgumentOutOfRangeException("animationGroup", "잘못된 애니메이션 그룹 번호입니다.");

            return animations[animationGroup];
        }

        public void SetAnimationGroupData(int animationGroup, SpriteStarAnimationData data)
        {
            if (animationGroup < 0 || animationGroup >= MaxAnimationGroup)
                throw new ArgumentOutOfRangeException("animationGroup", "잘못된 애니메이션 그룹 번호입니다.");

            groupData[animationGroup] = data;
        }

        public void Draw(Camera camera, Second deltaTime)
        {
            foreach (var a in animations)
            {
                a.Update(deltaTime);
            }

            sourceBillboards.Seek(0);
            flareBillboards.Seek(0);
            var cameraRotation = GetFlareAngleFromCamera(camera);
            for (int i = 0; i < sourceData.Length; i++)
            {
                SpriteStarData d = sourceData[i];
                SpriteStarAnimationData a = groupData[d.AnimationGroup];
                sourceBillboards.Append(
                    d.Position,
                    Color4.White,   // 광원은 무조건 흰색으로 색상을 입히지 않는다.
                    null,
                    new Vector2(0.5f, 0.5f),
                    new Vector2(d.LightSourceRadius * a.Scale),
                    cameraRotation + a.Rotation
                );
                flareBillboards.Append(
                    d.Position,
                    d.Color,
                    null,
                    new Vector2(0.5f, 0.5f),
                    new Vector2(d.FlareRadius * a.Scale),
                    cameraRotation + a.Rotation
                );
            }

            var shader = ShaderProvider.SimpleTexture;
            shader.Bind(ref camera.Matrix, lightSourceTexture);
            sourceVertexBuffer.Bind(sourceBillboards.Vertices);
            elementBuffer.Bind();
            GLHelper.DrawTriangles(elementBuffer.Count);

            shader.Bind(ref camera.Matrix, flareTexture);
            flareVertexBuffer.Bind(flareBillboards.Vertices);
            elementBuffer.Bind();
            GLHelper.DrawTriangles(elementBuffer.Count);
        }

        public void Dispose()
        {
            if (sourceVertexBuffer != null) sourceVertexBuffer.Dispose(); sourceVertexBuffer = null;
            if (flareVertexBuffer != null) flareVertexBuffer.Dispose(); flareVertexBuffer = null;
        }

        #region privates

        Radian GetFlareAngleFromCamera(Rendering.Camera camera)
        {
            Vector2 pos = camera.Position;
            return new Radian((pos.X - pos.Y * 0.75f) / 500.0f);
        }

        SpriteStarData[] sourceData;
        SpriteStarAnimation[] animations;
        SpriteStarAnimationData[] groupData;
        Texture lightSourceTexture;
        Texture flareTexture;
        VertexBuffer sourceVertexBuffer;
        VertexBuffer flareVertexBuffer;
        BillboardBuilder sourceBillboards;
        BillboardBuilder flareBillboards;
        ElementBuffer elementBuffer;

        #endregion
    }
}
