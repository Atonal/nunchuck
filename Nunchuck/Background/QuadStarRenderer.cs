﻿using OpenTK.Graphics.OpenGL;
using OpenTK;
using System;
using Nunchuck.Rendering;

namespace Nunchuck.Background
{
    public class QuadStarRenderer : IDisposable
    {
        public QuadStarRenderer(GameSystem gameSystem, SimpleStarData[] data)
        {
            var builder = new BillboardBuilder(data.Length);
            for (int i = 0; i < data.Length; i++)
            {
                var d = data[i];
                builder.Append(d.Position, d.Color);
            }

            shader = new Shader();
            vbo = VertexBuffer.CreateStatic(builder.Vertices, shader);
            ebo = ElementBuffer.Create(builder.Elements);

            this.gameSystem = gameSystem;
        }

        public void Draw(ref Matrix4 transform)
        {
            shader.Bind(ref transform, gameSystem.Viewport);
            vbo.Bind();
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public void Dispose()
        {
            vbo.Dispose();
            ebo.Dispose();
            shader.Dispose();
        }

        class Shader : ShaderBase
        {
            const string vertexSource = @"
                #version 150 core
                
                uniform mat4 trans;
				uniform vec2 viewPort;

                in vec3 positionIn;
                in vec4 colorIn;
                in vec2 textureIn;
                
                out vec4 color;
                out vec2 texture;
                
                void main()
                {
                    color = colorIn;
                    texture = textureIn;

                    vec4 p = vec4(positionIn, 1.0);
					p = trans * p;

					// normalized coord -> screen coord
					p.x = floor((p.x + 1) / 2 * viewPort.x / p.w) + textureIn.x * 2;
					p.y = floor((p.y + 1) / 2 * viewPort.y / p.w) + textureIn.y * 2;
					
					// screen coord -> normalized coord
					p.x = (p.x * p.w / viewPort.x) * 2 - 1;
					p.y = (p.y * p.w / viewPort.y) * 2 - 1;

                    gl_Position = p;
                }
            ";

            const string fragmentSource = @"
                #version 150 core

                in vec4 color;
                in vec2 texture;

                out vec4 colorOut;

                void main()
                {
					colorOut = color;
					if(!(texture.x <= 1.0 && texture.x >= 0.5)){
						colorOut.x = colorOut.x * 0.4;
						colorOut.y = colorOut.y * 0.4;
						colorOut.z = colorOut.z * 0.4;
					} 
					if(!(texture.y <= 0.5 && texture.y >=0.0)){
						colorOut.x = colorOut.x * 0.4;
						colorOut.y = colorOut.y * 0.4;
						colorOut.z = colorOut.z * 0.4;
					} 
                }
            ";

            public Shader()
                : base(vertexSource, fragmentSource)
            {
            }

            protected override void GetUniformLocation()
            {
                transPtr = GL.GetUniformLocation(ShaderProgram, "trans");
				viewPortPtr = GL.GetUniformLocation(ShaderProgram, "viewPort");   
            }

            public void Bind(ref Matrix4 transform, Vector2 viewPort)
            {
                base.Bind();
                GL.UniformMatrix4(transPtr, false, ref transform);
				GL.Uniform2(viewPortPtr, ref viewPort);
            }

            int transPtr;
			int viewPortPtr;
        }

        VertexBuffer vbo;
        ElementBuffer ebo;
        Shader shader;
        GameSystem gameSystem;
    }
}
