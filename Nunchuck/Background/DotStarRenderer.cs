﻿using OpenTK.Graphics.OpenGL;
using OpenTK;
using System;
using System.Diagnostics;
using Nunchuck.Rendering.VertexType;
using Nunchuck.Rendering;

namespace Nunchuck.Background
{
    public class DotStarRenderer : IDisposable
    {
        public DotStarRenderer(SimpleStarData[] data)
        {
            VertexPositionColor[] vertices = new VertexPositionColor[data.Length];
            int stride = BlittableValueType.StrideOf(vertices);
            for (int i = 0; i < data.Length; i++)
            {
                vertices[i].Position = data[i].Position;
                vertices[i].Color = data[i].Color;
            }

            shader = new Shader();
            vbo = VertexBuffer.CreateStatic(vertices, shader);

            GL.BindVertexArray(0);
        }

        public void Draw(ref Matrix4 transform)
        {
            shader.Bind(ref transform);
            vbo.Bind();
            ElementBuffer.Unbind();
            GL.DrawArrays(PrimitiveType.Points, 0, vbo.Count);
        }

        public void Dispose()
        {
            vbo.Dispose();
            shader.Dispose();
        }

        class Shader : ShaderBase
        {
            const string vertexSource = @"
                #version 150 core
                
                uniform mat4 trans;

                in vec3 positionIn;
                in vec4 colorIn;
                
                out vec4 color;
                
                void main()
                {
                    color = colorIn;
                    gl_Position = trans * vec4(positionIn, 1.0);
                }
            ";

            const string fragmentSource = @"
                #version 150 core

                in vec4 color;
                out vec4 colorOut;

                void main()
                {
                    colorOut = color;
                }
            ";

            public Shader()
                : base(vertexSource, fragmentSource)
            {
            }

            protected override void GetUniformLocation()
            {
                transPtr = GL.GetUniformLocation(ShaderProgram, "trans");   
            }

            public void Bind(ref Matrix4 transform)
            {
                base.Bind();
                GL.UniformMatrix4(transPtr, false, ref transform);
            }

            int transPtr;
        }

        VertexBuffer vbo;
        Shader shader;
    }
}
