﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace Nunchuck.Background
{
    public class DoodadBuilder
    {
        /// <param name="textureList">string texture1, [int weight1], string texture2, [int weight], ...</param>
        public void SetTextureSet(params object[] textureList)
        {
            textureSet.Clear();

            int i = 0;
            while (i < textureList.Length)
            {
                string texture = textureList[i++] as string;
                if (texture == null) { throw new ArgumentOutOfRangeException("잘못된 텍스쳐 목록 형식입니다."); }

                int weight = 1;
                if (i < textureList.Length && textureList[i] is int)
                {
                    weight = (int)(textureList[i++]);
                }
                textureSet.Add(texture, weight);
            }
        }

        public void SetScaleRange(float min, float max)
        {
            scale = new FloatRange(min, max);
        }

        public void SetRandomRotation(bool randomize)
        {
            this.randomRotation = randomize;
        }

        public void AddDoodad(DoodadData data)
        {
            doodadData.Add(data);
        }

        public void AddRandom(Vector3 position, DoodadLayer layer, int seed)
        {
            if (textureSet.IsEmpty()) { throw new InvalidOperationException("먼저 사용할 텍스쳐 집합을 지정해야 합니다."); }
            AddRandom(position, layer, new Random(seed));
        }

        public void AddUniform(float minX, float maxX, float minY, float maxY, float minZ, float maxZ, DoodadLayer layer, int count, int seed)
        {
            var rand = new Random(seed);
            for (int i = 0; i < count; i++)
            {
                Vector3 pos;
                pos.X = rand.NextFloat(minX, maxX);
                pos.Y = rand.NextFloat(minY, maxY);
                pos.Z = rand.NextFloat(minZ, maxZ);
                AddRandom(pos, layer, rand);
            }
        }

        public void AddCircular(Vector3 center, Vector3 radius, Vector3 rotation, DoodadLayer layer, int count, int seed)
        {
            var rand = new Random(seed);
            var rot = MatrixHelper.RotationYawPitchRoll(rotation);
            for (int i = 0; i < count; i++)
            {
                float r = (float)Math.Sqrt(rand.NextFloat(0, 1));
                Radian theta = (Radian)rand.NextFloat(0, Radian.Pi);
                Radian rho = (Radian)rand.NextFloat(0, Radian.Pi * 2);
                Vector4 unrotated = new Vector4(
                    r * radius.X * Radian.Cos(theta) * Radian.Cos(rho),
                    r * radius.Y * Radian.Sin(theta) * Radian.Cos(rho),
                    r * radius.Z * Radian.Sin(rho),
                    0
                );
                Vector4 rotated = Vector4.Transform(unrotated, rot);
                AddRandom(rotated.Xyz + center, layer, rand);
            }
        }

        public DoodadData[] FlushData()
        {
            var ret = doodadData.ToArray();
            doodadData.Clear();
            return ret;
        }

        #region privates

        void AddRandom(Vector3 position, DoodadLayer layer, Random rand)
        {
            if (textureSet.IsEmpty()) { throw new InvalidOperationException("먼저 사용할 텍스쳐 집합을 지정해야 합니다."); }
            var d = new DoodadData();
            d.Layer = layer;
            d.Position = position;
            d.Scale = rand.NextFloat(scale.Min, scale.Max);
            d.Texture = textureSet.GetRandom(rand);
            d.Rotation = (randomRotation) ? (Radian)(rand.NextFloat(Radian.Pi * 2)) : (Radian)0;
            doodadData.Add(d);
        }

        List<DoodadData> doodadData = new List<DoodadData>();
        Mathmatics.WeightedRandomPool<string> textureSet = new Mathmatics.WeightedRandomPool<string>();
        FloatRange scale = new FloatRange(1, 1);
        bool randomRotation = false;

        #endregion
    }
}
