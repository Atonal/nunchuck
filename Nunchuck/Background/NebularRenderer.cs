﻿using Nunchuck.Rendering;
using OpenTK;
using System;

namespace Nunchuck.Background
{
    public class NebularRenderer : IDisposable
    {
        public NebularRenderer(TextureCache textureCache, NebularData[] data)
        {
            var builder = new BillboardBuilder(data.Length);
            for (int i = 0; i < data.Length; i++)
            {
                var d = data[i];
                builder.Append(d.Position,
                    d.Color,
                    d.NoiseIndex.TextureUV,
                    new Vector2(0.5f, 0.5f),
                    d.Size,
                    d.Rotation);
            }
            vbo = VertexBuffer.CreateStatic(builder.Vertices, ShaderProvider.SimpleTexture);
            ebo = ElementBuffer.Create(builder.Elements);
            noise = textureCache.GetTexture("Resource/Background/NebulaNoise.png");
        }

        public void Draw(ref Matrix4 transform)
        {
            ShaderProvider.SimpleTexture.Bind(ref transform, noise);
            vbo.Bind();
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public void Dispose()
        {
            if (vbo != null) vbo.Dispose(); vbo = null;
            if (ebo != null) ebo.Dispose(); ebo = null;
        }

        Texture noise;
        VertexBuffer vbo;
        ElementBuffer ebo;
    }
}
