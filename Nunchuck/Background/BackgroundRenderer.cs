﻿using Nunchuck.Rendering;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;

namespace Nunchuck.Background
{
    public class BackgroundRenderer : IDisposable
    {
        public BackgroundRenderer(GameSystem gameSystem, BackgroundData data)
        {
            dotStar = new DotStarRenderer(data.DotStars ?? new SimpleStarData[0]);
            quadStar = new QuadStarRenderer(gameSystem, data.QuadStars ?? new SimpleStarData[0]);
            nebula = new NebularRenderer(gameSystem.TextureCache, data.Nebulas ?? new NebularData[0]);
            spriteStar = new SpriteStarRenderer(gameSystem.TextureCache, data.SpriteStars ?? new SpriteStarData[0]);
            for (int i = 0; i < 4; i++)
            {
                spriteStar.GetAnimation(i).SetAngularAnimation((Radian)0.5f, 0);
            }
            spriteStar.GetAnimation(0).SetScaleAnimation(0.70f, 1.00f, 1.15f, 0.00f);
            spriteStar.GetAnimation(1).SetScaleAnimation(0.70f, 1.00f, 1.15f, 0.75f);
            spriteStar.GetAnimation(2).SetScaleAnimation(0.70f, 1.00f, 1.00f, 0.35f);
            spriteStar.GetAnimation(3).SetScaleAnimation(0.85f, 1.00f, 2.00f, 0.35f);
            doodad = new DoodadRenderer(gameSystem, data.Doodads ?? new DoodadData[0]);
        }

        public void Draw(Camera camera, Second deltaTime)
        {
			GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
			dotStar.Draw(ref camera.Matrix);
			quadStar.Draw(ref camera.Matrix);
			nebula.Draw(ref camera.Matrix);
			spriteStar.Draw(camera, deltaTime);

            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			doodad.Draw(ref camera.Matrix);
        }

        public void Dispose()
        {
            if (dotStar != null) dotStar.Dispose(); dotStar = null;
            if (quadStar != null) quadStar.Dispose(); quadStar = null;
            if (nebula != null) nebula.Dispose(); nebula = null;
            if (spriteStar != null) spriteStar.Dispose(); spriteStar = null;
            if (doodad != null) doodad.Dispose(); doodad = null;
        }

        DotStarRenderer dotStar;
        QuadStarRenderer quadStar;
        NebularRenderer nebula;
        SpriteStarRenderer spriteStar;
        DoodadRenderer doodad;
    }
}
