﻿using Nunchuck.Mathmatics;
using Nunchuck.Rendering;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;

namespace Nunchuck.Background
{
    public class DoodadRenderer : IDisposable
    {
        public DoodadRenderer(GameSystem gameSystem, DoodadData[] doodads)
        {
            InitLayerProperty();
            InitAtlasScaleValue();

            System.Array.Sort(doodads,
                (d1, d2) => { return (int)(d1.Position.Z - d2.Position.Z); }
            );

            // Texture Atlas를 위한 임시 정보들
            Dictionary<string, Bitmap> loadedBitmaps = new Dictionary<string, Bitmap>();
            Dictionary<AtlasKey, RectangleF> atlasedInfo = new Dictionary<AtlasKey, RectangleF>(); ;
            textureAtlas = new TextureAtlas();

            BillboardBuilder builder = new BillboardBuilder(doodads.Length);
            for (int i = 0; i < doodads.Length; i++)
            {
                var z = doodads[i].Position.Z;
                var layerProperty = layerProperties[doodads[i].Layer];

                Bitmap bitmap;
                var path = doodads[i].Texture;
                if (!loadedBitmaps.TryGetValue(path, out bitmap))
                {
                    if (!File.Exists(path)) throw new FileNotFoundException("파일을 찾을 수 없습니다: " + path);
                    bitmap = new Bitmap(path);
                    loadedBitmaps[path] = bitmap;
                }

                var targetDensity = layerProperty.PixelDensity.GetValueAt(z);
                var atlasScale = doodads[i].Scale * targetDensity;
                atlasScale = atlasScaleValue.Quntize(atlasScale);
                atlasScale = Math.Max(atlasScale, 16.0f / bitmap.Height);

                RectangleF texUV;
                AtlasKey atlasKey = new AtlasKey(){ Bitmap = bitmap, Scale = atlasScale};
                if (!atlasedInfo.TryGetValue(atlasKey, out texUV))
                {
                    texUV = textureAtlas.Register(bitmap, atlasScale);
                    atlasedInfo[atlasKey] = texUV;
                }

                // Z값에 다른 스케일 변화 보존
                float scale = doodads[i].Scale;
                scale = -scale * z / gameSystem.Viewport.Y;

                // z값에 따른 밝기, 선명도, 채도 보정 값 결정
                float brightness = layerProperty.Brightness.GetValueAt(z);
                float sharpness = layerProperty.Sharpness.GetValueAt(z);
                float saturation = layerProperty.Saturation.GetValueAt(z);
                builder.Append(doodads[i].Position,
                    new Color4(brightness, sharpness, saturation, 1),
                    texUV,
                    new Vector2(0.5f, 0.5f),
                    (textureAtlas.Size * texUV.Size.ToVector2()) * scale / atlasScale,
                    doodads[i].Rotation);
            }

            foreach(var bitmap in loadedBitmaps.Values)
                bitmap.Dispose();
            loadedBitmaps.Clear();

            textureAtlas.BakeTexture();
            shader = new Shader();
            vbo = VertexBuffer.CreateStatic(builder.Vertices, shader);
            ebo = ElementBuffer.Create(builder.Elements);
        }

        public void Draw(ref Matrix4 transform)
        {
            shader.Bind(ref transform, textureAtlas.Size, textureAtlas.Texture);
            vbo.Bind();
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public void Dispose()
        {
            if (textureAtlas != null) textureAtlas.Dispose(); textureAtlas = null;
            if (vbo != null) vbo.Dispose(); vbo = null;
            if (ebo != null) ebo.Dispose(); ebo = null;
            if (shader != null) shader.Dispose(); shader = null;
        }

        #region privates

        void InitLayerProperty()
        {
            var back = new LayerProperty();
            back.PixelDensity.AddControlPoint(ZOrder.GameDepth, 1.00f);
            back.PixelDensity.AddControlPoint(ZOrder.BackDepth.Min, 0.80f);
            back.PixelDensity.AddControlPoint(ZOrder.BackDepth.Max, 0.25f);
            back.Brightness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            back.Brightness.AddControlPoint(ZOrder.BackDepth.Min, 0.80f);
            back.Brightness.AddControlPoint(ZOrder.BackDepth.Max, 0.25f);
            back.Sharpness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            back.Sharpness.AddControlPoint(ZOrder.BackDepth.Min, 0.85f);
            back.Sharpness.AddControlPoint(ZOrder.BackDepth.Max, 0);
            back.Saturation.AddControlPoint(ZOrder.GameDepth, 1.00f);
            back.Saturation.AddControlPoint(ZOrder.BackDepth.Min, 0.90f);
            back.Saturation.AddControlPoint(ZOrder.BackDepth.Max, 0.50f);
            layerProperties[DoodadLayer.Back] = back;

            var middle = new LayerProperty();
            middle.PixelDensity.AddControlPoint(ZOrder.GameDepth, 1.00f);
            middle.PixelDensity.AddControlPoint(ZOrder.MiddleDepth.Min, 0.80f);
            middle.PixelDensity.AddControlPoint(ZOrder.MiddleDepth.Max, 0.50f);
            middle.PixelDensity.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            middle.Brightness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            middle.Brightness.AddControlPoint(ZOrder.MiddleDepth.Min, 0.50f);
            middle.Brightness.AddControlPoint(ZOrder.MiddleDepth.Max, 0.25f);
            middle.Brightness.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            middle.Sharpness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            middle.Sharpness.AddControlPoint(ZOrder.MiddleDepth.Min, 0.75f);
            middle.Sharpness.AddControlPoint(ZOrder.MiddleDepth.Max, 0.25f);
            middle.Sharpness.AddControlPoint(ZOrder.MinStarDepth, 0);
            middle.Saturation.AddControlPoint(ZOrder.GameDepth, 1.00f);
            middle.Saturation.AddControlPoint(ZOrder.MiddleDepth.Min, 0.90f);
            middle.Saturation.AddControlPoint(ZOrder.MiddleDepth.Max, 0.50f);
            middle.Saturation.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            layerProperties[DoodadLayer.Middle] = middle;

            var front = new LayerProperty();
            front.PixelDensity.AddControlPoint(ZOrder.GameDepth, 1.00f);
            front.PixelDensity.AddControlPoint(ZOrder.FrontDepth.Min, 0.90f);
            front.PixelDensity.AddControlPoint(ZOrder.FrontDepth.Max, 0.80f);
            front.PixelDensity.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            front.Brightness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            front.Brightness.AddControlPoint(ZOrder.FrontDepth.Min, 0.85f);
            front.Brightness.AddControlPoint(ZOrder.FrontDepth.Max, 0.55f);
            front.Brightness.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            front.Sharpness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            front.Sharpness.AddControlPoint(ZOrder.FrontDepth.Min, 0.90f);
            front.Sharpness.AddControlPoint(ZOrder.FrontDepth.Max, 0.75f);
            front.Sharpness.AddControlPoint(ZOrder.MinStarDepth, 0);
            front.Saturation.AddControlPoint(ZOrder.GameDepth, 1.00f);
            front.Saturation.AddControlPoint(ZOrder.FrontDepth.Min, 0.90f);
            front.Saturation.AddControlPoint(ZOrder.FrontDepth.Max, 0.50f);
            front.Saturation.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            layerProperties[DoodadLayer.Front] = front;

            foreach (DoodadLayer layer in Enum.GetValues(typeof(DoodadLayer)))
            {
                Debug.Assert(layerProperties.ContainsKey(layer));
            }
        }

        void InitAtlasScaleValue()
        {
            atlasScaleValue.AddValue(1.0f);
            atlasScaleValue.AddValue(0.75f);
            atlasScaleValue.AddValue(0.66f);
            atlasScaleValue.AddValue(0.5f);
            atlasScaleValue.AddValue(0.33f);
        }

        class LayerProperty
        {
            public PiecewiseLinearFunction PixelDensity = new PiecewiseLinearFunction();
            public PiecewiseLinearFunction Brightness = new PiecewiseLinearFunction();
            public PiecewiseLinearFunction Sharpness = new PiecewiseLinearFunction();
            public PiecewiseLinearFunction Saturation = new PiecewiseLinearFunction();
        };

        struct AtlasKey
        {
            public Bitmap Bitmap;
            public float Scale;
        };

        public class Shader : ShaderBase
        {
            const string vertexSource = @"
                #version 150 core
                
                uniform mat4 trans;

                in vec3 positionIn;
                in vec4 colorIn;
                in vec2 textureIn;
                
                out vec4 color;
                out vec2 texCoord;
                
                void main()
                {
                    color = colorIn;
                    texCoord = textureIn;
                    gl_Position = trans * vec4(positionIn, 1);
                }
            ";

            const string fragmentSource = @"
                #version 150 core

                uniform sampler2D tex;
                uniform vec2 texSize;

                in vec4 color;
                in vec2 texCoord;

                out vec4 colorOut;

                void main()
                {
                    float brightness = color.r;
                    float sharpness = color.g;
                    float saturation = color.b;
                    float dx = 1 / texSize.x;
                    float dy = 1 / texSize.y;
                    vec4 sum = vec4(0, 0, 0, 0);
                    sum += texture(tex, texCoord + vec2(-dx, -dy));
                    sum += texture(tex, texCoord + vec2(0, -dy));
                    sum += texture(tex, texCoord + vec2(dx, -dy));
                    sum += texture(tex, texCoord + vec2(-dx, 0));
                    sum += texture(tex, texCoord + vec2(0, 0));
                    sum += texture(tex, texCoord + vec2(dx, 0));
                    sum += texture(tex, texCoord + vec2(-dx, dy));
                    sum += texture(tex, texCoord + vec2(0, dy));
                    sum += texture(tex, texCoord + vec2(dx, dy));
                    sum += texture(tex, texCoord + vec2(-dx * 2, 0));
                    sum += texture(tex, texCoord + vec2(0, -dy * 2));
                    sum += texture(tex, texCoord + vec2(dx * 2, 0));
                    sum += texture(tex, texCoord + vec2(0, dy * 2));
                    vec4 blur = sum / 13 * (1 - sharpness) + texture(tex, texCoord) * sharpness;
                    vec3 col = blur.rgb;
                    vec3 luminanceWeights = vec3(0.299, 0.587, 0.114);
                    float luminance = dot(col, luminanceWeights);
                    col = luminance * (1 - saturation) + col * saturation;
                    colorOut = vec4(col * brightness, blur.a);
                }
            ";

            public Shader()
                : base(vertexSource, fragmentSource)
            {
            }

            protected override void GetUniformLocation()
            {
                transPtr = GL.GetUniformLocation(ShaderProgram, "trans");
                texSizePtr = GL.GetUniformLocation(ShaderProgram, "texSize");
            }

            public void Bind(ref Matrix4 transform, Vector2 textureSize, Texture texture)
            {
                base.Bind();
                GL.UniformMatrix4(transPtr, false, ref transform);
                GL.Uniform2(texSizePtr, ref textureSize);
                GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
            }

            int transPtr;
            int texSizePtr;
        }

        Dictionary<DoodadLayer, LayerProperty> layerProperties = new Dictionary<DoodadLayer, LayerProperty>();
        Mathmatics.Quantizer atlasScaleValue = new Mathmatics.Quantizer();
        TextureAtlas textureAtlas;
        VertexBuffer vbo;
        ElementBuffer ebo;
        Shader shader;

        #endregion
    }
}
