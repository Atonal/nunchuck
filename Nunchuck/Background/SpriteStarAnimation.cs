﻿
namespace Nunchuck.Background
{
    public class SpriteStarAnimation
    {
        public SpriteStarAnimation(SpriteStarRenderer renderer, int animationGroup)
        {
            this.renderer = renderer;
            this.animationGroup = animationGroup;
            this.angularSpeed = (Radian)0;
            this.minScale = 1.0f;
            this.maxScale = 1.0f;
            this.scaleInterval = 1.0f;
            this.currentAngle = (Radian)0;
            this.currentScale = 0;
        }

        public void SetAngularAnimation(Radian speed, float offset)
        {
            angularSpeed = speed;
            currentAngle = speed * offset;
        }

        public void SetScaleAnimation(float min, float max, float interval, float offset)
        {
            minScale = min;
            maxScale = max;
            currentScaleOffset = offset;
            scaleInterval = interval;
        }

        public void Update(Second deltaTime)
        {
            currentAngle += angularSpeed * (float)deltaTime;

            currentScaleOffset += deltaTime;
            while (currentScaleOffset > scaleInterval)
                currentScaleOffset -= scaleInterval;
            currentScale = LinearScale(minScale, maxScale, scaleInterval, ref currentScaleOffset);
            renderer.SetAnimationGroupData(animationGroup, new SpriteStarAnimationData()
            {
                Rotation = currentAngle,
                Scale = currentScale
            });
        }

        #region privates

        float LinearScale(float min, float max, float interval, ref float offset)
        {
            while (offset > interval)
                offset -= interval;
            float scale = offset / interval;
            if (scale < 0.5f)
                scale = scale * 2;
            else
                scale = (1.0f - scale) * 2;

            return scale * (max - min) + min;
        }

        SpriteStarRenderer renderer;
        int animationGroup;

        #region animation state

        Radian angularSpeed;
        float minScale;
        float maxScale;
        float scaleInterval;
        
        Radian currentAngle;
        float currentScaleOffset;
        float currentScale;

        #endregion

        #endregion
    }
}
