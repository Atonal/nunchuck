﻿using OpenTK;
using OpenTK.Graphics;

namespace Nunchuck.Background
{
    public struct NebularData
    {
        public Vector3 Position;
        public Color4 Color;
        public Vector2 Size;
        public Radian Rotation;
        public NebularNoiseIndex NoiseIndex;
    }
}
