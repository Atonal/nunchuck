﻿using OpenTK;
using System;
using System.Drawing;

namespace Nunchuck
{
    public static partial class Extensions
    {
        public static uint ToRgba(this Color color)
        {
            return (uint)color.A << 24 | (uint)color.B << 16 | (uint)color.G << 8 | (uint)color.R;
        }

        public static Vector3 ToVector3(this Color color)
        {
            return new Vector3(
                color.R / 255.0f,
                color.G / 255.0f,
                color.B / 255.0f
            );
        }

        public static Vector4 ToVector4(this Color color)
        {
            return new Vector4(
                color.R / 255.0f,
                color.G / 255.0f,
                color.B / 255.0f,
                color.A / 255.0f
            );
        }

        public static Vector2 Transform(this Matrix2 matrix, Vector2 v)
        {
            return new Vector2(
                matrix.M11 * v.X + matrix.M12 * v.Y,
                matrix.M21 * v.X + matrix.M22 * v.Y);
        }

        public static float NextFloat(this Random rand, float max)
        {
            return (float)(rand.NextDouble() * max);
        }

        public static float NextFloat(this Random rand, float min, float max)
        {
            return (float)(rand.NextDouble() * (max - min) + min);
        }

        public static Vector2 GetTopLeft(this RectangleF rect)
        {
            return new Vector2(rect.Left, rect.Top);
        }

        public static Vector2 GetTopRight(this RectangleF rect)
        {
            return new Vector2(rect.Right, rect.Top);
        }

        public static Vector2 GetBottomLeft(this RectangleF rect)
        {
            return new Vector2(rect.Left, rect.Bottom);
        }

        public static Vector2 GetBottomRight(this RectangleF rect)
        {
            return new Vector2(rect.Right, rect.Bottom);
        }

        public static Vector2 ToVector2(this Size size)
        {
            return new Vector2(size.Width, size.Height);
        }

        public static Vector2 ToVector2(this SizeF size)
        {
            return new Vector2(size.Width, size.Height);
        }
    }
}
