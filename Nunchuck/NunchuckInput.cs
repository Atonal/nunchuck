﻿using System;
using System.IO;
using System.IO.Ports;
using System.Threading;

namespace Nunchuck
{
    class NunchuckInput : IDisposable
    {
        public class HardwareAbstraction
        {
            public const int ClockInMs = 50; // used any number, to be corrected in future
            public const int DataLengthInBytes = 16;
            public const char PaddingCharacter = '\n';
        }

        public NunchuckInput(string portName)
        {
            port = new SerialPort(portName);
            if (!port.IsOpen)
                port.Open();
            readThread = new Thread(BeginRead);
            readThread.Start();
        }

        public static NunchuckInput Open(string portName)
        {
            return new NunchuckInput(portName);
        }

        public static NunchuckInput Open()
        {
            var names = SerialPort.GetPortNames();
            if (names == null || names.Length == 0)
                throw new NotSupportedException("No serial port founded");
            return Open(names[0]);
        }

        private void BeginRead()
        {
            AlignSerialData();
            ReadLoop();
        }

        private void AlignSerialData()
        {
            const int maxAlignCycle = 7;
            const int alignCycle = 3;
            byte[] alignBuffer = new byte[HardwareAbstraction.DataLengthInBytes * maxAlignCycle];
            Thread.Sleep(HardwareAbstraction.ClockInMs * alignCycle);

            int readPosition = 0;
            int alignEnd = alignBuffer.Length;
            while (readPosition < alignEnd)
            {
                readPosition += port.Read(alignBuffer, readPosition, alignEnd - readPosition);

                bool alignEndCheck = (alignEnd == alignBuffer.Length)
                    && (readPosition > alignCycle * HardwareAbstraction.DataLengthInBytes);
                Func<int, bool> isPaddingIndex = (i) =>
                {
                    int alignCount = 0;
                    while (i < readPosition)
                    {
                        if (alignBuffer[i] == HardwareAbstraction.PaddingCharacter)
                        {
                            alignCount++;
                        }
                        else
                        {
                            return false;
                        }
                        i += HardwareAbstraction.DataLengthInBytes;
                    }
                    return alignCount >= alignCycle;
                };
                if (alignEndCheck)
                {
                    for (int i = 0; i < HardwareAbstraction.DataLengthInBytes; i++)
                    {
                        if (alignBuffer[i] == HardwareAbstraction.PaddingCharacter)
                        {
                            if (isPaddingIndex(i))
                            {
                                const int len = HardwareAbstraction.DataLengthInBytes;
                                int endCycle = ((readPosition + len - 1) / len);
                                alignEnd = i + endCycle * len + 1;
                            }
                        }
                    }
                }
            }
        }

        private void ReadLoop()
        {
            try
            {
                while (true)
                {
                    ButtonC = DeserializeChar() != '0';
                    ButtonZ = DeserializeChar() != '0';
                    JoyX = DeserializeByte();
                    JoyY = DeserializeByte();
                    AccX = Deserialize12Bit();
                    AccY = Deserialize12Bit();
                    AccZ = Deserialize12Bit();
                    var padding = DeserializeChar();
                    NewInputArrived();
                }
            }
            catch (IOException)
            {
            }
        }

        public bool ButtonC { get; private set; }
        public bool ButtonZ { get; private set; }
        public int JoyX { get; private set; }
        public int JoyY { get; private set; }
        public int AccX { get; private set; }
        public int AccY { get; private set; }
        public int AccZ { get; private set; }

        public event Action NewInputArrived;

        private char DeserializeChar()
        {
            int readByte = 0;
            while (true)
            {
                readByte = port.Read(buffer, 0, 1);
                if (readByte != 0)
                    break;
                Thread.Yield();
            }
            return (char)buffer[0];
        }

        private byte DeserializeDigit()
        {
            char ch = DeserializeChar();
            if (char.IsNumber(ch))
                return (byte)(ch - '0');
            else if (char.IsLetter(ch) && 'a' <= ch)
                return (byte)(ch - 'a' + 0x0a);
            else
                throw new InvalidOperationException("Input was expected as hexadecimal value, but unknown type character was given.");
        }

        private byte DeserializeByte()
        {
            byte d1 = DeserializeDigit();
            byte d2 = DeserializeDigit();
            return (byte)((d1 << 4) | d2);
        }

        private int Deserialize12Bit()
        {
            byte d1 = DeserializeDigit();
            byte d2 = DeserializeDigit();
            byte d3 = DeserializeDigit();
            return (d1 << 8) | (d2 << 4) | d3;
        }

        public void Dispose()
        {
            if (port != null)
            {
                if (port.IsOpen)
                    port.Close();
                port = null;
            }

            if (readThread != null)
            {
                readThread.Abort();
                readThread = null;
            }
        }

        byte[] buffer = new byte[HardwareAbstraction.DataLengthInBytes];
        Thread readThread;
        SerialPort port;
    }
}
