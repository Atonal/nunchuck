﻿using Nunchuck.Logic;
using Nunchuck.Rendering;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;

namespace Nunchuck
{
    public class GameWindow : OpenTK.GameWindow
    {
        public GameWindow(GameSystem gameSystem)
            : base(1280, 720
            , GraphicsMode.Default
            , "Nunchuck"
            , GameWindowFlags.FixedWindow
            , DisplayDevice.Default
            , 3, 2
            , GraphicsContextFlags.ForwardCompatible | GraphicsContextFlags.Debug)
        {
            this.gameSystem = gameSystem;
            this.VSync = VSyncMode.Off;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var bgData = new Background.Factory.Background().Create();
            background = new Background.BackgroundRenderer(gameSystem, bgData);

            camera = new Camera(gameSystem.Viewport);
            inputState = new InputState();
            RestartSession();
        }

        void RestartSession()
        {
            gameSession = new GameSession(gameSystem);
            gameSession.RestartRequested += RestartSession;
            gameSession.Start();
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            var deltaTime = (Second)e.Time;

            inputState.Update(deltaTime);
            gameSession.Update(deltaTime, inputState);

            var keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Key.F4) && keyboard.IsKeyDown(Key.AltLeft))
                Close();

            base.OnUpdateFrame(e);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.ClearColor(Color4.Black);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.Blend);

            var deltaTime = (Second)e.Time;
            background.Draw(camera, deltaTime);
            gameSession.Draw();

            SwapBuffers();
            GLHelper.CheckGLError();
            base.OnRenderFrame(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            inputState.Stop();

            base.OnClosed(e);
        }

        Camera camera;
        Background.BackgroundRenderer background;
        GameSystem gameSystem;
        GameSession gameSession;
        InputState inputState;
    }
}
