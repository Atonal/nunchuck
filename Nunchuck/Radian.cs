﻿using OpenTK;
using System;

namespace Nunchuck
{
    public struct Radian
    {
        private float value;
        public static readonly Radian Zero = new Radian(0);
        public static readonly Radian Pi = new Radian((float)Math.PI);

        #region .ctor
        public Radian(float radian)
        {
            value = radian;
        }
        public Radian(Degree degree)
        {
            value = (float)degree / 180.0f * Pi;
        }
        #endregion

        #region trigonometric
        public static float Cos(Radian r)
        {
            return (float)Math.Cos(r.value);
        }
        public static float Sin(Radian r)
        {
            return (float)Math.Sin(r.value);
        }
        public static float Tan(Radian r)
        {
            return (float)Math.Tan(r.value);
        }
        #endregion
        #region trigonometric advanced
        public Vector2 Rotate(Vector2 v)
        {
            return new Vector2(
                (float)(v.X * Math.Cos(value) - v.Y * Math.Sin(value)),
                (float)(v.X * Math.Sin(value) + v.Y * Math.Cos(value))
            );
        }
        public static Vector2 ToDirection(Radian r)
        {
            return new Vector2(Cos(r), Sin(r));
        }
        #endregion

        #region casting
        public static explicit operator Radian(float radian)
        {
            return new Radian(radian);
        }

        public static implicit operator Radian(Degree degree)
        {
            return new Radian(degree);
        }

        public static implicit operator float(Radian radian)
        {
            return radian.value;
        }
        #endregion
        #region operator
        public static Radian operator -(Radian a)
        {
            return new Radian(-a.value);
        }

        public static Radian operator +(Radian a, Radian b)
        {
            return new Radian(a.value + b.value);
        }

        public static Radian operator +(Radian a, float b)
        {
            return new Radian(a.value + b);
        }

        public static Radian operator +(float a, Radian b)
        {
            return new Radian(a + b.value);
        }

        public static Radian operator -(Radian a, Radian b)
        {
            return new Radian(a.value - b.value);
        }

        public static Radian operator -(float a, Radian b)
        {
            return new Radian(a - b.value);
        }

        public static Radian operator -(Radian a, float b)
        {
            return new Radian(a.value - b);
        }

        public static Radian operator /(Radian a, Radian b)
        {
            return new Radian(a.value / b.value);
        }

        public static Radian operator /(Radian a, float b)
        {
            return new Radian(a.value / b);
        }

        public static Radian operator /(float a, Radian b)
        {
            return new Radian(a / b.value);
        }

        public static Radian operator *(Radian a, Radian b)
        {
            return new Radian(a.value * b.value);
        }

        public static Radian operator *(Radian a, float b)
        {
            return new Radian(a.value * b);
        }

        public static Radian operator *(float a, Radian b)
        {
            return new Radian(a * b.value);
        }
#endregion
    }
}
