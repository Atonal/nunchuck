﻿using Nunchuck.Rendering;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Nunchuck
{
    public class GameSystem : IDisposable
    {
        public readonly GameWindow Window;

        public TextureCache TextureCache { get; private set; }

        public Vector2 Viewport
        {
            get
            {
                return new Vector2(Window.Width, Window.Height);
            }
        }

        public GameSystem()
        {
            InitializeCurrentDirectory();

            Window = new GameWindow(this);
            disposeList.Push(Window);

            TextureCache = new TextureCache();
            disposeList.Push(TextureCache);

            GLHelper.SetViewport(Viewport);
        }

        void InitializeCurrentDirectory()
        {
            // Resource 폴더를 일일이 복사하지 않더라도 실행 경로를 수정. 테스트 가능하도록
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Resource"))
            {
                if (Debugger.IsAttached || Directory.Exists(Environment.CurrentDirectory + "\\..\\..\\Resource"))
                {
                    Environment.CurrentDirectory = "../../";
                }
            }
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Resource"))
            {
                throw new FileNotFoundException("리소스 파일의 경로를 찾지 못했습니다.\n현재 경로: " + Environment.CurrentDirectory + "\n");
            }
        }

        public void Run()
        {
            Window.Run();
        }

        #region IDisposable

        public void Dispose()
        {
            while (disposeList.Count > 0)
                disposeList.Pop();
        }

        Stack<IDisposable> disposeList = new Stack<IDisposable>();

        #endregion
    }
}
