﻿
namespace Nunchuck
{
    public struct Degree
    {
        public Degree(float degree)
        {
            value = degree;
        }
        public Degree(Radian radian)
        {
            value = radian / Radian.Pi * 180.0f;
        }

        public static explicit operator Degree(float radian)
        {
            return new Degree(radian);
        }

        public static implicit operator Degree(Radian radian)
        {
            return new Degree(radian);
        }

        public static explicit operator float(Degree degree)
        {
            return degree.value;
        }

        public static Degree operator +(Degree a, Degree b)
        {
            return new Degree(a.value + b.value);
        }

        public static Degree operator -(Degree a, Degree b)
        {
            return new Degree(a.value - b.value);
        }

        public static Degree operator /(Degree a, float f)
        {
            return new Degree(a.value / f);
        }

        public static Degree operator *(Degree a, float f)
        {
            return new Degree(a.value * f);
        }

        public static Degree operator *(float f, Degree a)
        {
            return a * f;
        }

        private float value;
    }
}
