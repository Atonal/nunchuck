﻿using OpenTK;

namespace Nunchuck.Rendering
{
    public class Effect
    {
        public Effect(Texture[] textures, Second frameInterval)
        {
            this.textures = textures;
            this.timeToNextFrame = frameInterval;
            this.frameInterval = frameInterval;
            currentFrame = 0;
            IsAlive = true;
        }

        public void Update(Second deltaTime)
        {
            timeToNextFrame -= deltaTime;
            while (timeToNextFrame < Second.Zero)
            {
                currentFrame++;
                timeToNextFrame += frameInterval;
            }
            if (currentFrame >= textures.Length)
                IsAlive = false;
        }

        public void Draw()
        {
            GLHelper.DrawTexture(textures[currentFrame],
                Position,
                null, null,
                new Vector2(0.5f, 0.5f),
                null,
                null);
        }

        public Vector2 Position;
        public bool IsAlive { get; private set; }

        int currentFrame = 0;
        Second frameInterval;
        Second timeToNextFrame;
        Texture[] textures;
    }
}
