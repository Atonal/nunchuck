﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Nunchuck.Rendering
{
    public class Texture : IDisposable
    {
        public Texture(int texture, Vector2 size)
        {
            this.texture = texture;
            this.size = size;
        }

        public static Texture CreateFromFile(string path)
        {
            if (!File.Exists(path)) 
                throw new FileNotFoundException("텍스쳐 파일을 찾을 수 없습니다.\npath=" + path);

            using (Bitmap bitmap = new Bitmap(path))
            {
                return CreateFromBitmap(bitmap);
            }
        }

        public static Texture CreateFromBitmap(Bitmap bitmap)
        {
            var texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texture);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            BitmapData bitmapData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
            ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmapData.Width, bitmapData.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitmapData.Scan0);
            bitmap.UnlockBits(bitmapData);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            return new Texture(texture, new Vector2(bitmap.Width, bitmap.Height));
        }

        public static Texture CreateEmpty(Vector2 size)
        {
            var texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texture);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, (int)size.X, (int)size.Y, 0, 
                OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
            //GL.BindTexture(TextureTarget.Texture2D, 0);

            return new Texture(texture, size);
        }

        public void Dispose()
        {
            if (texture != 0) GL.DeleteTexture(texture); texture = 0;
        }

        public Vector2 Size
        {
            get
            {
                return size;
            }
        }

        public int TextureHandle
        {
            get
            {
                return texture;
            }
        }

        #region privates

        int texture;
        Vector2 size;

        #endregion
    }
}
