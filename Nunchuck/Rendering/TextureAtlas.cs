﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;

namespace Nunchuck.Rendering
{
    public class TextureAtlas : IDisposable
    {
        public const int MaxTextureSize = 2048;

        public TextureAtlas()
        {
            bitmap = new Bitmap(MaxTextureSize, MaxTextureSize);
            graphics = Graphics.FromImage(bitmap);
            emptyList = new List<Point>();
            emptyList.Add(Point.Empty);
        }

        public RectangleF Register(string path, float scale = 1.0f)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("파일을 찾을 수 없습니다.\npath=" + path);

            using (Bitmap bitmap = new Bitmap(path))
            {
                int width = (int)Math.Ceiling(bitmap.Width * scale);
                int height = (int)Math.Ceiling(bitmap.Height * scale);
                return Register(bitmap, new Size(width, height));
            }
        }

        public RectangleF Register(string path, Size targetSize)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("파일을 찾을 수 없습니다.\npath=" + path);

            using (Bitmap bitmap = new Bitmap(path))
            {
                return Register(bitmap, targetSize);
            }
        }

        public RectangleF Register(Bitmap bitmap, Size targetSize)
        {
            DeleteTexture();
            var allocated = Allocate(targetSize);
            allocatedList.Add(allocated);
            graphics.DrawImage(bitmap, allocated);
            var ret = new RectangleF(
                (float)allocated.X / MaxTextureSize,
                (float)allocated.Y / MaxTextureSize,
                (float)allocated.Width / MaxTextureSize,
                (float)allocated.Height / MaxTextureSize
            );
            return ret;
        }

        public RectangleF Register(Bitmap bitmap, float scale = 1.0f)
        {
            int width = (int)Math.Ceiling(bitmap.Width * scale);
            int height = (int)Math.Ceiling(bitmap.Height * scale);
            return Register(bitmap, new Size(width, height));
        }

        public void BakeTexture()
        {
            if (bakedTexture == null)
            {
                bakedTexture = Texture.CreateFromBitmap(bitmap);
            }
        }

        public void Dispose()
        {
            DeleteTexture();
            if (graphics != null) graphics.Dispose(); graphics = null;
            if (bitmap != null) bitmap.Dispose(); bitmap = null;
        }

        public Texture Texture
        {
            get
            {
                Debug.Assert(bakedTexture != null,
                    "Texture를 쓰기 전에 먼저 BakeTexture를 호출해야 합니다.");
                return bakedTexture;
            }
        }

        public Vector2 Size
        {
            get
            {
                return bitmap.Size.ToVector2();
            }
        }

        #region privates

        void DeleteTexture()
        {
            if (bakedTexture != null) bakedTexture.Dispose(); bakedTexture = null;
        }

        #region area allocation

        Rectangle Allocate(Size size)
        {
            int optimalIndex = emptyList.Count;
            int optimalHeight = bitmap.Height;

            // 추가 가능한 영역들 중 높이가 가장 작은 영역을 검색
            for (int i = 0; i < emptyList.Count - 1; i++)
            {
                int emptyHeight = emptyList[i + 1].Y - emptyList[i].Y;
                if (emptyHeight < optimalHeight
                    && emptyHeight >= size.Height
                    && IsAllocatable(emptyList[i], size))
                {
                    optimalIndex = i;
                    optimalHeight = emptyHeight;
                }
            }

            // 최적 영역의 높이와 입력 높이가 너무 많이 차이나는 경우 예외 처리
            if (optimalHeight * 0.75 > size.Height
                && IsAllocatable(emptyList[emptyList.Count - 1], size))
            {
                optimalIndex = emptyList.Count;
            }

            // 새로운 영역으로 추가해야 하는 상황이지만,
            // 마지막 영역의 높이와 입력 높이가 별로 차이나지 않는 경우 예외처리
            if (emptyList.Count == optimalIndex && emptyList.Count > 1)
            {
                int last = emptyList.Count - 1;
                int lastHeight = emptyList[last].Y - emptyList[last - 1].Y;
                if (size.Height > lastHeight
                    && lastHeight * 1.10 > size.Height
                    && IsAllocatable(emptyList[last - 1], size))
                {
                    Rectangle rect = new Rectangle()
                    {
                        Location = emptyList[last - 1],
                        Size = size
                    };
                    emptyList[last - 1] = new Point(rect.Right + padding, rect.Top);
                    emptyList[last] = new Point(0, rect.Bottom + padding);
                    return rect;
                }
            }

            if (emptyList.Count == optimalIndex)
            {
                // 아래쪽에 새로운 영역으로 추가
                Rectangle rect = new Rectangle()
                {
                    Location = emptyList[emptyList.Count - 1],
                    Size = size
                };
                if (IsAllocatable(rect))
                {
                    emptyList.RemoveAt(emptyList.Count - 1);
                    emptyList.Add(new Point(rect.Right + padding, rect.Top));
                    emptyList.Add(new Point(0, rect.Bottom + padding));
                    return rect;
                }
            }
            else
            {
                // 기존 영역의 오른쪽에 추가
                Rectangle rect = new Rectangle()
                {
                    Location = emptyList[optimalIndex],
                    Size = size
                };
                if (IsAllocatable(rect))
                {
                    emptyList[optimalIndex] = new Point(rect.Right + padding, rect.Top);
                    return rect;
                }
            }

            throw new InvalidOperationException("TextureAtlas에 더 이상 공간이 없습니다.");
        }

        bool IsAllocatable(Point position, Size size)
        {
            Rectangle rect = new Rectangle()
            {
                Location = position,
                Size = size
            };
            return IsAllocatable(rect);
        }

        bool IsAllocatable(Rectangle rect)
        {
            if (rect.Right > bitmap.Width || rect.Bottom > bitmap.Height)
                return false;

            foreach (Rectangle r in allocatedList)
            {
                if (rect.IntersectsWith(r)) return false;
            }
            return true;
        }

        #endregion

        Texture bakedTexture;
        Bitmap bitmap;
        Graphics graphics;
        List<Point> emptyList;
        List<Rectangle> allocatedList = new List<Rectangle>();
        const int padding = 2;

        #endregion
    }
}
