﻿using OpenTK;
using System;

namespace Nunchuck.Rendering
{
    public class Camera
    {
        public const float Far = 100000.0f;
        public const float Near = 1.0f;

        public Camera(Vector2 viewport)
        {
            view = Matrix4.Identity;
            FitViewTo(viewport);
            MoveTo(new Vector2(0, 0));
        }

        public void MoveTo(Vector2 position)
        {
            this.position = position;
            view = Matrix4.CreateTranslation(
                new Vector3(-position.X, -position.Y, 0)
            );
            Matrix4.Mult(ref view, ref projection, out Matrix);
            Matrix4.Invert(ref Matrix, out MatrixInverse);
        }

        public void MoveTo(Vector3 position)
        {
            MoveTo(new Vector2(position.X, position.Y));
        }

        public void FitViewTo(Vector2 viewport)
        {
            this.viewport = viewport;
            float fieldOfView = Radian.Pi / 4.0f;
            float screenAspect = (float)viewport.X / viewport.Y;
            Matrix4.CreatePerspectiveFieldOfView(
                fieldOfView,
                screenAspect,
                Near,
                Far,
                out projection
            );
            Matrix4.Mult(ref view, ref projection, out Matrix);
            Matrix4.Invert(ref Matrix, out MatrixInverse);
        }

        public Vector3 ScreenToWorld(Vector2 screen, float z)
        {
            screen.X = (screen.X / viewport.X) * 2 - 1;
            screen.Y = (screen.Y / viewport.Y) * 2 - 1;
            var trZ = Vector4.Transform(new Vector4(0, 0, z, 1), Matrix).Z;
            var scr4 = new Vector4(screen.X * -z, screen.Y * -z, trZ, -z);
            var wrd4 = Vector4.Transform(scr4, MatrixInverse);
            return new Vector3(wrd4.X, wrd4.Y, z);
        }

        public Vector2 WorldToScreen(Vector3 world)
        {
            var scr4 = Vector4.Transform(new Vector4(world, 1), Matrix);
            var scr = new Vector2(scr4.X, scr4.Y) / scr4.W;
            scr = (scr + new Vector2(1, 1)) / 2;
            scr = scr * viewport;
            return scr;
        }

        public Matrix4 Matrix;
        public Matrix4 MatrixInverse;

        public Vector2 Position
        {
            get
            {
                return position;
            }
        }

        Matrix4 view;
        Matrix4 projection;
        Vector2 position;
        Vector2 viewport;
    }
}
