﻿using System;

namespace Nunchuck.Rendering.VertexType
{
    public delegate void VertexSetupFunction(int shaderProgram);

    public static class VertexTypes
    {
        static class VertexSetupFunctionHolder<T>
        {
            static VertexSetupFunctionHolder()
            {
                var methodInfo = typeof(T).GetMethod("Setup");
                if (methodInfo == null)
                {
                    throw new Exception("모든 정점 구조체는 Setup을 정의하고 있어야 합니다.");
                }
                setupFunction = (VertexSetupFunction)Delegate.CreateDelegate(typeof(VertexSetupFunction), methodInfo);
            }

            static readonly VertexSetupFunction setupFunction;
            public static VertexSetupFunction SetupFunction
            {
                get
                {
                    return setupFunction;
                }
            }
        }

        public static VertexSetupFunction SetupFunctionOf<T>(T[] type)
        {
            return VertexSetupFunctionHolder<T>.SetupFunction;
        }
    }

}
