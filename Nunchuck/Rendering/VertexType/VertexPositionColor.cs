﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;

namespace Nunchuck.Rendering.VertexType
{
    public struct VertexPositionColor
    {
        public Vector3 Position;
        public Color4 Color;

        public static void Setup(int shaderProgram)
        {
            int stride = BlittableValueType<VertexPositionColor>.Stride;

            int posAttr = GL.GetAttribLocation(shaderProgram, "positionIn");
            if (posAttr >= 0)
            {
                GL.EnableVertexAttribArray(posAttr);
                GL.VertexAttribPointer(posAttr, 3, VertexAttribPointerType.Float, false, stride, 0);
            }

            int colAttr = GL.GetAttribLocation(shaderProgram, "colorIn");
            if (colAttr >= 0)
            {
                GL.EnableVertexAttribArray(colAttr);
                GL.VertexAttribPointer(colAttr, 4, VertexAttribPointerType.Float, false, stride, 3 * sizeof(float));
            }

            GLHelper.CheckGLError();
        }
    }
}
