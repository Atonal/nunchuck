﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Nunchuck.Rendering.VertexType
{
    public struct VertexScreenPositionColorTexture
    {
        public Vector2 Position;
        public Color4 Color;
        public Vector2 TextureUV;

        public static void Setup(int shaderProgram)
        {
            int stride = BlittableValueType<VertexScreenPositionColorTexture>.Stride;

            int posAttr = GL.GetAttribLocation(shaderProgram, "positionIn");
            if (posAttr >= 0)
            {
                GL.EnableVertexAttribArray(posAttr);
                GL.VertexAttribPointer(posAttr, 2, VertexAttribPointerType.Float, false, stride, 0);
            }

            int colAttr = GL.GetAttribLocation(shaderProgram, "colorIn");
            if (colAttr >= 0)
            {
                GL.EnableVertexAttribArray(colAttr);
                GL.VertexAttribPointer(colAttr, 4, VertexAttribPointerType.Float, false, stride, 2 * sizeof(float));
            }

            int texAttr = GL.GetAttribLocation(shaderProgram, "textureIn");
            if (texAttr >= 0)
            {
                GL.EnableVertexAttribArray(texAttr);
                GL.VertexAttribPointer(texAttr, 2, VertexAttribPointerType.Float, false, stride, 6 * sizeof(float));
            }
            GLHelper.CheckGLError();
        }
    }
}
