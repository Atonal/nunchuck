﻿using Nunchuck.Rendering.VertexType;
using OpenTK;
using OpenTK.Graphics;
using System.Drawing;

namespace Nunchuck.Rendering
{
    public class BillboardBuilder
    {
        public const int VertexPerQuad = 4;
        public const int ElementPerQuad = 6;

        public BillboardBuilder(int quadCount, BillboardBuilderOption option = BillboardBuilderOption.Default)
        {
            vertices = new VertexPositionColorTexture[quadCount * VertexPerQuad];
            if (option != BillboardBuilderOption.NoElementBuffer)
            {
                elements = new int[quadCount * ElementPerQuad];
                var ePos = 0;
                for (int i = 0; i < quadCount; i++)
                {
                    elements[ePos++] = i * VertexPerQuad + 0;
                    elements[ePos++] = i * VertexPerQuad + 2;
                    elements[ePos++] = i * VertexPerQuad + 1;
                    elements[ePos++] = i * VertexPerQuad + 2;
                    elements[ePos++] = i * VertexPerQuad + 1;
                    elements[ePos++] = i * VertexPerQuad + 3;
                }
            }
            vPos = 0;
        }

        public void Append(Vector3 position, Color4 color)
        {
            vertices[vPos++] = new VertexPositionColorTexture()
            {
                Position = position, 
                Color = color,
                TextureUV = new Vector2(0, 0)
            };
            vertices[vPos++] = new VertexPositionColorTexture()
            {
                Position = position,
                Color = color,
                TextureUV = new Vector2(1, 0)
            };
            vertices[vPos++] = new VertexPositionColorTexture()
            {
                Position = position,
                Color = color,
                TextureUV = new Vector2(0, 1)
            };
            vertices[vPos++] = new VertexPositionColorTexture()
            {
                Position = position,
                Color = color,
                TextureUV = new Vector2(1, 1)
            };
        }

        public void Append(Vector3 position,
            Color4? colorOrWhite, RectangleF? textureUVOrUnitRect,
            Vector2? anchorOrZero, Vector2? sizeOrOne, Radian? rotationOrZero)
        {
            var color = colorOrWhite ?? Color4.White;
            var textureUV = textureUVOrUnitRect ?? new RectangleF(0, 0, 1, 1);
            var anchor = anchorOrZero ?? Vector2.Zero;
            var size = sizeOrOne ?? Vector2.One;
            var rotation = rotationOrZero ?? (Radian)0.0f;

            Vector2 tl = new Vector2(-anchor.X * size.X, -anchor.Y * size.Y);
            Vector2 tr = new Vector2((1 - anchor.X) * size.X, -anchor.Y * size.Y);
            Vector2 bl = new Vector2(-anchor.X * size.X, (1 - anchor.Y) * size.Y);
            Vector2 br = new Vector2((1 - anchor.X) * size.X, (1 - anchor.Y) * size.Y);
            tl = rotation.Rotate(tl);
            tr = rotation.Rotate(tr);
            bl = rotation.Rotate(bl);
            br = rotation.Rotate(br);

            VertexPositionColorTexture vertex;
            vertex.Color = color;
            vertex.TextureUV = textureUV.GetTopLeft();
            vertex.Position = position;
            vertex.Position.X += tl.X;
            vertex.Position.Y += tl.Y;
            vertices[vPos++] = vertex;

            vertex.TextureUV = textureUV.GetTopRight();
            vertex.Position = position;
            vertex.Position.X += tr.X;
            vertex.Position.Y += tr.Y;
            vertices[vPos++] = vertex;

            vertex.TextureUV = textureUV.GetBottomLeft();
            vertex.Position = position;
            vertex.Position.X += bl.X;
            vertex.Position.Y += bl.Y;
            vertices[vPos++] = vertex;

            vertex.TextureUV = textureUV.GetBottomRight();
            vertex.Position = position;
            vertex.Position.X += br.X;
            vertex.Position.Y += br.Y;
            vertices[vPos++] = vertex;
        }

        public void Append(Vector3 position,
            Color4? colorOrWhite, RectangleF? textureUVOrUnitRect,
            Vector2? anchorOrZero, Vector2? sizeOrOne, Vector2 rotationDirection)
        {
            var color = colorOrWhite ?? Color4.White;
            var textureUV = textureUVOrUnitRect ?? new RectangleF(0, 0, 1, 1);
            var anchor = anchorOrZero ?? Vector2.Zero;
            var size = sizeOrOne ?? Vector2.One;
            var dir = rotationDirection;

            Vector2 tl = new Vector2(-anchor.X * size.X, -anchor.Y * size.Y);
            Vector2 tr = new Vector2((1 - anchor.X) * size.X, -anchor.Y * size.Y);
            Vector2 bl = new Vector2(-anchor.X * size.X, (1 - anchor.Y) * size.Y);
            Vector2 br = new Vector2((1 - anchor.X) * size.X, (1 - anchor.Y) * size.Y);
            Matrix2 rotation = new Matrix2(dir.X, dir.Y, dir.Y, -dir.X);
            tl = rotation.Transform(tl);
            tr = rotation.Transform(tr);
            bl = rotation.Transform(bl);
            br = rotation.Transform(br);

            VertexPositionColorTexture vertex;
            vertex.Color = color;
            vertex.TextureUV = textureUV.GetTopLeft();
            vertex.Position = position;
            vertex.Position.X += tl.X;
            vertex.Position.Y += tl.Y;
            vertices[vPos++] = vertex;

            vertex.TextureUV = textureUV.GetTopRight();
            vertex.Position = position;
            vertex.Position.X += tr.X;
            vertex.Position.Y += tr.Y;
            vertices[vPos++] = vertex;

            vertex.TextureUV = textureUV.GetBottomLeft();
            vertex.Position = position;
            vertex.Position.X += bl.X;
            vertex.Position.Y += bl.Y;
            vertices[vPos++] = vertex;

            vertex.TextureUV = textureUV.GetBottomRight();
            vertex.Position = position;
            vertex.Position.X += br.X;
            vertex.Position.Y += br.Y;
            vertices[vPos++] = vertex;
        }

        public VertexPositionColorTexture[] Vertices
        {
            get
            {
                return vertices;
            }
        }

        public int[] Elements
        {
            get
            {
                return elements;
            }
        }

        public int VertexCount
        {
            get
            {
                return vertices.Length;
            }
        }

        public int ElementCount
        {
            get
            {
                return elements.Length;
            }
        }

        public int ElementPosition
        {
            get
            {
                return vPos / VertexPerQuad * ElementPerQuad;
            }
        }

        public int QuadIndex
        {
            get
            {
                return vPos / VertexPerQuad;
            }
        }

        public void Seek(int quadIndex)
        {
            this.vPos = quadIndex * VertexPerQuad;
        }

        #region privates

        VertexPositionColorTexture[] vertices;
        int[] elements;
        int vPos;

        #endregion
    }
}
