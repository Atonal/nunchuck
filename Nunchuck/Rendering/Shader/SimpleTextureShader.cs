﻿using Nunchuck.Rendering.VertexType;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Nunchuck.Rendering.Shader
{
    public class SimpleTextureShader : ShaderBase
    {
        const string vertexSource = @"
                #version 150 core
                
                uniform mat4 trans;

                in vec3 positionIn;
                in vec4 colorIn;
                in vec2 textureIn;
                
                out vec4 color;
                out vec2 texCoord;
                
                void main()
                {
                    color = colorIn;
                    texCoord = textureIn;
                    gl_Position = trans * vec4(positionIn, 1);
                }
            ";

        const string fragmentSource = @"
                #version 150 core

                uniform sampler2D tex;

                in vec4 color;
                in vec2 texCoord;

                out vec4 colorOut;

                void main()
                {
                    colorOut = texture(tex, texCoord) * color;
                }
            ";

        public SimpleTextureShader()
            : base(vertexSource, fragmentSource)
        {
        }

        protected override void GetUniformLocation()
        {
            transPtr = GL.GetUniformLocation(ShaderProgram, "trans");
        }

        public void Bind(ref Matrix4 transform, Texture texture)
        {
            base.Bind();
            GL.UniformMatrix4(transPtr, false, ref transform);
            GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
        }

        int transPtr;
    }
}
