﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;

namespace Nunchuck.Rendering
{
    public class VertexBuffer : IDisposable
    {
        private VertexBuffer(int vbo, int vao, int count, VertexBufferType type)
        {
            this.vbo = vbo;
            this.vao = vao;
            this.count = count;
            this.type = type;
        }

        public static VertexBuffer CreateStatic<T>(T[] vertices, ShaderBase shader) where T:struct
        {
            var vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            var count = vertices.Length;
            var stride = BlittableValueType.StrideOf(vertices);
            var vbo = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(stride * count), vertices, BufferUsageHint.StaticDraw);
            
            CheckUploadState(count, stride);
            VertexType.VertexTypes.SetupFunctionOf(vertices)(shader.ShaderProgram);
            GLHelper.CheckGLError();

            return new VertexBuffer(vbo, vao, count, VertexBufferType.Static);
        }

        public static VertexBuffer CreateDynamic<T>(T[] vertices, ShaderBase shader) where T : struct
        {
            var vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            var count = vertices.Length;
            var stride = BlittableValueType.StrideOf(vertices);
            var vbo = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            VertexType.VertexTypes.SetupFunctionOf(vertices)(shader.ShaderProgram);
            GLHelper.CheckGLError();
            return new VertexBuffer(vbo, vao, count, VertexBufferType.Dynamic);
        }

        public void Bind<T>(T[] vertices) where T : struct
        {
            Debug.Assert(type == VertexBufferType.Dynamic);
            Debug.Assert(vertices.Length <= count);

            var stride = BlittableValueType.StrideOf(vertices);

            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * stride), IntPtr.Zero, BufferUsageHint.StreamDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * stride), vertices, BufferUsageHint.StreamDraw);
        }

        public void Bind()
        {
            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
        }

        public void Dispose()
        {
            if (vbo != 0) GL.DeleteBuffer(vbo); vbo = 0;
            if (vao != 0) GL.DeleteVertexArray(vao); vao = 0;
        }

        public int Count
        {
            get
            {
                return count;
            }
        }

        #region privates

        [Conditional("DEBUG")]
        static void CheckUploadState(int count, int stride)
        {
            int size;
            GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out size);
            if (count * stride != size)
                throw new ApplicationException("Vertex data not uploaded correctly");
        }

        int vao;
        int vbo;
        int count;
        VertexBufferType type;

        #endregion
    }
}
