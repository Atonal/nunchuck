﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Nunchuck.Rendering
{
    public abstract class ShaderBase : IDisposable
    {
        public ShaderBase(string vSource, string fSource)
        {
            CreateVertexShader(vSource);
            CreateFregmentShader(fSource);
            LinkShader();
            GetUniformLocation();
        }

        #region private Helpers

        void CreateVertexShader(string source)
        {
            vertexShader = GL.CreateShader(ShaderType.VertexShader);
            CompileShader(vertexShader, source);
        }

        void CreateFregmentShader(string source)
        {
            fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            CompileShader(fragmentShader, source);
        }

        void CompileShader(int shader, string source)
        {
            string shaderLog;
            int shaderStatusCode;

            GL.ShaderSource(shader, source);
            GL.CompileShader(shader);
            GL.GetShaderInfoLog(shader, out shaderLog);
            GL.GetShader(shader, ShaderParameter.CompileStatus, out shaderStatusCode);
            if (shaderStatusCode == 0)
            {
                var stackTrace = new StackTrace(true);
                var stackFrames = stackTrace.GetFrame(4);
                var fileName = Path.GetFileNameWithoutExtension(stackFrames.GetFileName());
                MessageBox.Show(string.Format("'{0}'에서 셰이더 컴파일에 실패했습니다.\n{1}",
                    fileName, shaderLog));
            }
        }

        void LinkShader()
        {
            shaderProgram = GL.CreateProgram();
            GL.AttachShader(shaderProgram, vertexShader);
            GL.AttachShader(shaderProgram, fragmentShader);
            GL.BindFragDataLocation(shaderProgram, 0, "colorOut");
            GL.LinkProgram(shaderProgram);
        }

        #endregion

        protected abstract void GetUniformLocation();

        protected void Bind()
        {
            GL.UseProgram(shaderProgram);
        }

        public void Dispose()
        {
            if (vertexShader != 0) GL.DeleteShader(vertexShader); vertexShader = 0;
            if (fragmentShader != 0) GL.DeleteShader(fragmentShader); fragmentShader = 0;
            if (shaderProgram != 0) GL.DeleteProgram(shaderProgram); shaderProgram = 0;
        }

        public int ShaderProgram
        {
            get
            {
                return shaderProgram;
            }
        }

        int vertexShader;
        int fragmentShader;
        int shaderProgram;
    }
}
