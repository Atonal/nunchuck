﻿using Nunchuck.Rendering.Shader;

namespace Nunchuck.Rendering
{
    public static class ShaderProvider
    {
        public static void UnloadShader()
        {
            if (simpleTexture != null) simpleTexture.Dispose(); simpleTexture = null;
            if (screenSpaceTexture != null) screenSpaceTexture.Dispose(); screenSpaceTexture = null;
        }

        static SimpleTextureShader simpleTexture;
        public static SimpleTextureShader SimpleTexture
        {
            get
            {
                if (simpleTexture == null)
                {
                    simpleTexture = new SimpleTextureShader();
                }
                return simpleTexture;
            }
        }

        static ScreenSpaceTextureShader screenSpaceTexture;
        public static ScreenSpaceTextureShader ScreenSpaceTexture
        {
            get
            {
                if (screenSpaceTexture == null)
                {
                    screenSpaceTexture = new ScreenSpaceTextureShader();
                }
                return screenSpaceTexture;
            }
        }
    }
}
