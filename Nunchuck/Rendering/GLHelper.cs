﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;
using System.Drawing;

namespace Nunchuck.Rendering
{
    public static class GLHelper
    {
        [Conditional("DEBUG")]
        public static void CheckGLError()
        {
            var error = GL.GetError();
            Debug.Assert(error == ErrorCode.NoError);
        }

        /// <summary>
        /// 테스트 목적으로 있는 함수.
        /// 실제 기능 구현에는 사용하지 말 것
        /// </summary>
        public static void DrawTexture(Texture texture, Vector2 position, float scale = 1.0f)
        {
            Vector3 color = new Vector3(1.0f, 1.0f, 1.0f);
            DrawTexture(texture, position, 1.0f, color);
        }

        public static void DrawTexture(Texture texture, Vector2 position, float opacity, Vector3 color, float scale = 1.0f)
        {           
            Matrix3 mat = Matrix3.Identity;
            mat.M31 = position.X;
            mat.M32 = position.Y;
            DrawTexture(texture, mat, opacity, color);
        }

        public static void DrawTexture(Texture texture, Vector2 position, Color4? colorOrWhite, RectangleF? textureUVOrUnitRect,
            Vector2? anchorOrZero, Vector2? sizeOrOne, Radian? rotationOrZero)
        {
            var color = colorOrWhite ?? Color4.White;
            var textureUV = textureUVOrUnitRect ?? new RectangleF(0, 0, 1, 1);
            var anchor = anchorOrZero ?? Vector2.Zero;
            var size = (sizeOrOne ?? Vector2.One) * texture.Size;
            var rotation = rotationOrZero ?? (Radian)0.0f;

            Vector2 tl = new Vector2(-anchor.X * size.X, (1 - anchor.Y) * size.Y);
            Vector2 tr = new Vector2((1 - anchor.X) * size.X, (1 - anchor.Y) * size.Y);
            Vector2 bl = new Vector2(-anchor.X * size.X, -anchor.Y * size.Y);
            Vector2 br = new Vector2((1 - anchor.X) * size.X, -anchor.Y * size.Y);
            tl = rotation.Rotate(tl);
            tr = rotation.Rotate(tr);
            bl = rotation.Rotate(bl);
            br = rotation.Rotate(br);


            if (vbo == null)
            {
                vbo = VertexBuffer.CreateDynamic(vertices, ShaderProvider.ScreenSpaceTexture);
                int[] indices = new int[6];
                indices[0] = 0;
                indices[1] = 1;
                indices[2] = 2;
                indices[3] = 2;
                indices[4] = 1;
                indices[5] = 3;
                ebo = ElementBuffer.Create(indices);
            }

            vertices[0].TextureUV = textureUV.GetTopLeft();
            vertices[0].Position = position + tl;
            vertices[0].Color = color;

            vertices[1].TextureUV = textureUV.GetTopRight();
            vertices[1].Position = position + tr;
            vertices[1].Color = color;

            vertices[2].TextureUV = textureUV.GetBottomLeft();
            vertices[2].Position = position + bl;
            vertices[2].Color = color;

            vertices[3].TextureUV = textureUV.GetBottomRight();
            vertices[3].Position = position + br;
            vertices[3].Color = color;

            var identity = Matrix3.Identity;
            ShaderProvider.ScreenSpaceTexture.Bind(ref identity, viewport, texture);
            vbo.Bind(vertices);
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public static void DrawTexture(Texture texture, Matrix3 mat, float opacity, Vector3 color, float scale = 1.0f)
        {
            if (vbo == null)
            {
                vbo = VertexBuffer.CreateDynamic(vertices, ShaderProvider.ScreenSpaceTexture);
                int[] indices = new int[6];
                indices[0] = 0;
                indices[1] = 1;
                indices[2] = 2;
                indices[3] = 2;
                indices[4] = 1;
                indices[5] = 3;
                ebo = ElementBuffer.Create(indices);
            }

            //
            Vector2 pos = new Vector2(0.0f, 0.0f);
            Color4 c4 = new Color4(color.X, color.Y, color.Z, opacity);

            vertices[0].TextureUV = new Vector2(0, 1);
            vertices[0].Position = pos;
            vertices[0].Color = c4;

            vertices[1].TextureUV = new Vector2(1, 1);
            vertices[1].Position = pos + new Vector2(texture.Size.X * scale, 0);
            vertices[1].Color = c4;

            vertices[2].TextureUV = new Vector2(0, 0);
            vertices[2].Position = pos + new Vector2(0, texture.Size.Y * scale);
            vertices[2].Color = c4;

            vertices[3].TextureUV = new Vector2(1, 0);
            vertices[3].Position = pos + new Vector2(texture.Size.X * scale, texture.Size.Y * scale);
            vertices[3].Color = c4;

            ShaderProvider.ScreenSpaceTexture.Bind(ref mat, viewport, texture);
            vbo.Bind(vertices);
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public static void DrawTexture(Texture texture, Matrix3 mat, int width, int height, Color4 fgColor, RectangleF rectUV, float scale = 1.0f)
        {
            if (vbo == null)
            {
                vbo = VertexBuffer.CreateDynamic(vertices, ShaderProvider.ScreenSpaceTexture);
                int[] indices = new int[6];
                indices[0] = 0;
                indices[1] = 1;
                indices[2] = 2;
                indices[3] = 2;
                indices[4] = 1;
                indices[5] = 3;
                ebo = ElementBuffer.Create(indices);
            }

            Vector2 pos = new Vector2(0.0f, 0.0f);
            vertices[0].TextureUV = new Vector2(rectUV.X, rectUV.Y + rectUV.Height);
            vertices[0].Position = pos;
            vertices[0].Color = fgColor;

            vertices[1].TextureUV = new Vector2(rectUV.X + rectUV.Width, rectUV.Y + rectUV.Height);
            vertices[1].Position = pos + new Vector2(width * scale, 0);
            vertices[1].Color = fgColor;

            vertices[2].TextureUV = new Vector2(rectUV.X, rectUV.Y);
            vertices[2].Position = pos + new Vector2(0, height * scale);
            vertices[2].Color = fgColor;

            vertices[3].TextureUV = new Vector2(rectUV.X + rectUV.Width, rectUV.Y);
            vertices[3].Position = pos + new Vector2(width * scale, height * scale);
            vertices[3].Color = fgColor;
                
            ShaderProvider.ScreenSpaceTexture.Bind(ref mat, viewport, texture);
            vbo.Bind(vertices);
            ebo.Bind();
            GLHelper.DrawTriangles(ebo.Count);
        }

        public static void DrawTriangles(int count)
        {
            GL.DrawElements(PrimitiveType.Triangles, count, DrawElementsType.UnsignedInt, 0);
        }

        public static void Unload()
        {
            if (vbo != null) vbo.Dispose(); vbo = null;
            if (ebo != null) ebo.Dispose(); ebo = null;
        }

        public static void SetViewport(Vector2 viewport)
        {
            GLHelper.viewport = viewport;
        }

        #region privates

        static VertexType.VertexScreenPositionColorTexture[] vertices = new VertexType.VertexScreenPositionColorTexture[4];
        static VertexBuffer vbo;
        static ElementBuffer ebo;
        static Vector2 viewport;

        #endregion
    }
}
