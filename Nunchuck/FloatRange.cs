﻿using System.Diagnostics;

namespace Nunchuck
{
    public struct FloatRange
    {
        public FloatRange(float min, float max)
        {
            Debug.Assert(min <= max);
            this.min = min;
            this.max = max;
        }

        public void Set(float min, float max)
        {
            Debug.Assert(min <= max);
            this.min = min;
            this.max = max;
        }

        public float Min
        {
            get
            {
                return min;
            }
            set
            {
                Debug.Assert(value <= max);
                min = value;
            }
        }
        public float Max
        {
            get
            {
                return max;
            }
            set
            {
                Debug.Assert(min <= value);
                max = value;
            }
        }

        private float min;
        private float max;
    }
}
