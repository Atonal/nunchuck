﻿using Nunchuck.Rendering;
using OpenTK;
using System;
using System.Collections.Generic;

namespace Nunchuck.Logic
{
    public class GameSession
    {
        public GameSession(GameSystem gameSystem)
        {
            this.gameSystem = gameSystem;
            var textureCache = gameSystem.TextureCache;

            lifeTexture = textureCache.GetTexture("Resource\\UI\\lifeElement.png");
            ammoTexture = textureCache.GetTexture("Resource\\UI\\bullet.png");
            gameOverTexture = textureCache.GetTexture("Resource\\UI\\gameOver.png");
            pressAnyButtonTexture = textureCache.GetTexture("Resource\\UI\\pressAnyButton.png");
            aimTexture = textureCache.GetTexture("Resource\\UI\\aim.png");
            bombTexture = textureCache.GetTexture("Resource\\UI\\bomb.png");

            moveGuideTexture = textureCache.GetTexture("Resource\\UI\\move.png");
            attackGuideTexture = textureCache.GetTexture("Resource\\UI\\attack.png");
            specialGuideTexture = textureCache.GetTexture("Resource\\UI\\special.png");
            shakeGuideTexture = textureCache.GetTexture("Resource\\UI\\shake.png");

            playerFighterTexture = textureCache.GetTexture("Resource\\FighterR.png");
            PlayerFighter = new PlayerFighter(this, gameSystem.Viewport / 2);

            collisionShake = new CollisionShake();
            spawnRule = new SpawnRules(this);
            enemyFighterTexture = textureCache.GetTexture("Resource\\FighterG.png");

            bulletTexture = textureCache.GetTexture("Resource\\bullet.png");

            var explodeList = new List<Texture>();
            for (int i = 1; i <= 48; i++)
            {
                var path = string.Format("Resource\\Effect\\Explode\\explode{0:D4}.png", i);
                var texture = textureCache.GetTexture(path);
                explodeList.Add(texture);
            }
            explodeEffectTextures = explodeList.ToArray();

            state = State.Initialized;
        }

        public void Start()
        {
            if (state != State.Initialized)
                throw new InvalidOperationException();
            state = State.Running;
        }

        public void AddExplodeEffect(Vector2 position)
        {
            var effect = new Effect(explodeEffectTextures, (Second)(1.0f / 24));
            effect.Position = position;
            effects.Add(effect);
        }

        public void Update(Second deltaTime, IReadonlyInputState inputState)
        {
            if (state == State.Running)
            {
                PlayerFighter.Update(deltaTime, inputState);
                foreach (var e in enemies)
                    e.Update(deltaTime);

                UpdateBullets(deltaTime);

                CheckCollisionBetweenFighters();

                spawnRule.Update(deltaTime);

                BoundPlayerInGameArea();

                aliveTime += deltaTime;
            }

            if (state == State.Terminated)
            {
                gameOverTime += deltaTime;
                if (gameOverTime > gameOverDisplayDelay &&
                    (inputState.Attack || inputState.Special))
                {
                    RestartRequested();
                }
            }

            CheckCollisionBetweenBullet();

            UpdateCollisionShake(deltaTime);

            UpdateEffects(deltaTime);
        }


        void UpdateBullets(Second deltaTime)
        {
            bulletsToBeUpdated.Clear();
            foreach (var b in bullets)
            {
                b.Update(deltaTime);
                var pos = b.Position;
                if (pos.X >= 0
                    && pos.Y >= 0
                    && pos.X <= gameSystem.Viewport.X
                    && pos.Y <= gameSystem.Viewport.Y)
                {
                    bulletsToBeUpdated.Add(b);
                }
            }
            var temp = bullets;
            bullets = bulletsToBeUpdated;
            bulletsToBeUpdated = temp;
        }

        void CheckCollisionBetweenFighters()
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                var enemy = enemies[i];
                var player = PlayerFighter;
                if ((enemy.Position - player.Position).Length < playerFighterTexture.Size.X / 2)
                {
                    enemies.RemoveAt(i);
                    i--;
                    AddExplodeEffect(enemy.Position);

                    var stunTime = (Second)1.5f;
                    collisionShake.Shake(stunTime);
                    player.Stun(stunTime);

                    DamagePlayer(3);
                }
            }
        }

        void CheckCollisionBetweenBullet()
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                var e = enemies[i];
                for (int j = 0; j < bullets.Count; j++)
                {
                    var b = bullets[j];
                    if ((b.Position - e.Position).Length < enemyFighterTexture.Size.X / 2)
                    {
                        bullets.RemoveAt(j);
                        enemies.RemoveAt(i);
                        i--;
                        j--;
                        AddExplodeEffect(e.Position);
                        goto nextEnemy;
                    }
                }
            nextEnemy: ;
            }
        }

        void DamagePlayer(int amount)
        {
            PlayerFighter.Damage(amount);
            if (PlayerFighter.Life == 0)
            {
                state = State.Terminated;
                AddExplodeEffect(PlayerFighter.Position);
            }
        }

        void BoundPlayerInGameArea()
        {
            if (PlayerFighter.Position.X < 0
                || PlayerFighter.Position.Y < 0
                || PlayerFighter.Position.X > gameSystem.Viewport.X
                || PlayerFighter.Position.Y > gameSystem.Viewport.Y)
            {
                var stunTime = (Second)3.0;
                PlayerFighter.Stun((Second)3.0);
                collisionShake.Shake((Second)3.0);
                PlayerFighter.BoundPosition(
                    playerFighterTexture.Size / 2,
                    gameSystem.Viewport - playerFighterTexture.Size / 2);
            }
        }

        void UpdateCollisionShake(Second deltaTime)
        {
            if (!PlayerFighter.IsStunned)
                collisionShake.End();
            collisionShake.Update(deltaTime);
        }

        void UpdateEffects(Second deltaTime)
        {
            effectToBeUpdated.Clear();
            foreach (var e in effects)
            {
                e.Update(deltaTime);
                if (e.IsAlive)
                    effectToBeUpdated.Add(e);
            }
            var tmp = effectToBeUpdated;
            effectToBeUpdated = effects;
            effects = tmp;
        }

        public void AddEnemyFighter(EnemyFighter fighter)
        {
            enemies.Add(fighter);
        }

        public void AddBullet(Vector2 position, Vector2 velocity)
        {
            bullets.Add(new Bullet(position, velocity));
        }

        public void Draw()
        {
            foreach (var e in enemies)
            {
                GLHelper.DrawTexture(enemyFighterTexture,
                    e.Position,
                    null, null, new Vector2(0.5f, 0.5f), null,
                    e.Rotation);
            }

            foreach (var b in bullets)
            {
                GLHelper.DrawTexture(bulletTexture, b.Position,
                    null, null, new Vector2(0.5f, 0.5f), null, null);
            }

            if (state == State.Initialized || state == State.Running)
            {
                DrawHpBar();
                DrawAmmo();
                DrawBomb();

                GLHelper.DrawTexture(playerFighterTexture,
                    PlayerFighter.Position - collisionShake.ShakeValue,
                    null, null, new Vector2(0.5f, 0.5f), null,
                    PlayerFighter.Rotation);

                if (PlayerFighter.Target != null)
                {
                    GLHelper.DrawTexture(aimTexture,
                        PlayerFighter.Target.Position - collisionShake.ShakeValue,
                        null, null, new Vector2(0.5f, 0.5f), null, null);
                }
            }

            foreach (var e in effects)
            {
                e.Draw();
            }

            if (state != State.Terminated)
            {
                DrawGuides();
            }

            if (state == State.Terminated)
            {
                DrawGameOver();
            }
        }

        void DrawHpBar()
        {
            const float spacing = 5;
            const float width = spacing * PlayerFighter.MaxLife;
            Vector2 basePosition = PlayerFighter.Position;
            basePosition.Y += playerFighterTexture.Size.Y;
            basePosition.X -= width / 2;
            for (int i = 0; i < PlayerFighter.Life; i++)
            {
                GLHelper.DrawTexture(lifeTexture,
                    basePosition + new Vector2(spacing * i, 0),
                    null, null, new Vector2(0.5f, 0.5f), null, null);
            }
        }

        void DrawAmmo()
        {
            const float spacing = 7;
            const float height = spacing * PlayerFighter.MaxAmmo;
            Vector2 basePosition = PlayerFighter.Position;
            basePosition.Y -= height / 2;
            basePosition.X += playerFighterTexture.Size.X;
            for (int i = 0; i < PlayerFighter.BulletAmmo; i++)
            {
                GLHelper.DrawTexture(ammoTexture,
                    basePosition + new Vector2(0, spacing * i),
                    null, null, new Vector2(0, 0), null, null);
            }
        }

        void DrawBomb()
        {
            Vector2 basePosition = PlayerFighter.Position;
            if (PlayerFighter.BombAvailable)
            {
                GLHelper.DrawTexture(bombTexture,
                    basePosition,
                    null, null, new Vector2(0.5f, 0.5f), null,
                    (Radian)(Radian.Pi * (float)aliveTime));
            }
        }

        void DrawGameOver()
        {
            if (gameOverTime < gameOverDisplayDelay) return;

            var viewport = gameSystem.Viewport;
            var centerScreen = viewport / 2;

            GLHelper.DrawTexture(gameOverTexture,
                centerScreen + new Vector2(0, 50),
                null, null, new Vector2(0.5f, 0.5f), null, null);

            var tick = (int)((gameOverTime - gameOverDisplayDelay) * 1.5f);
            if (tick % 2 == 0)
            {
                GLHelper.DrawTexture(pressAnyButtonTexture,
                    centerScreen + new Vector2(0, -100),
                    null, null, new Vector2(0.5f, 0.5f), null, null);
            }
        }

        void DrawGuides()
        {
            var viewport = gameSystem.Viewport;
            var centerScreen = viewport / 2;

            if (!moveGuided)
            {
                if (PlayerFighter.Velocity != Vector2.Zero)
                    moveGuided = true;

                if (2.0f < aliveTime && aliveTime < 5.0f)
                {
                    GLHelper.DrawTexture(moveGuideTexture,
                        centerScreen + new Vector2(0, -100),
                        null, null, new Vector2(0.5f, 0.5f), null, null);
                }
                else if (aliveTime > 5.0f)
                {
                    moveGuided = true;
                }
                return;
            }

            if (PlayerFighter.IsStunned)
            {
                GLHelper.DrawTexture(shakeGuideTexture,
                    centerScreen + new Vector2(0, -100),
                    null, null, new Vector2(0.5f, 0.5f), null, null);
                return;
            }

            if (!attackGuided && enemies.Count > 0)
            {
                if (bullets.Count > 0)
                    attackGuided = true;

                if (attackGuideTime == Second.Zero)
                    attackGuideTime = aliveTime;

                if (attackGuideTime <= aliveTime && aliveTime <= attackGuideTime + 5.0f)
                {
                    GLHelper.DrawTexture(attackGuideTexture,
                        centerScreen + new Vector2(0, -100),
                        null, null, new Vector2(0.5f, 0.5f), null, null);
                }
                else
                {
                    attackGuided = true;
                }
                return;
            }

            if (!specialGuided && (PlayerFighter.BombAvailable || specialGuideTime != Second.Zero))
            {
                if (specialGuideTime == Second.Zero)
                    specialGuideTime = aliveTime;

                if (specialGuideTime <= aliveTime && aliveTime <= specialGuideTime + 5.0f)
                {
                    GLHelper.DrawTexture(specialGuideTexture,
                        centerScreen + new Vector2(0, -100),
                        null, null, new Vector2(0.5f, 0.5f), null, null);
                }
                else
                {
                    specialGuided = true;
                }
                return;
            }

        }

        public event Action RestartRequested = delegate { };

        GameSystem gameSystem;
        public PlayerFighter PlayerFighter { get; private set; }

        public IReadOnlyList<EnemyFighter> EnemyFighters
        {
            get
            {
                return enemies.AsReadOnly();
            }
        }

        List<EnemyFighter> enemies = new List<EnemyFighter>();
        List<Effect> effects = new List<Effect>();
        List<Effect> effectToBeUpdated = new List<Effect>();
        List<Bullet> bullets = new List<Bullet>();
        List<Bullet> bulletsToBeUpdated = new List<Bullet>();
        Texture playerFighterTexture;
        Texture enemyFighterTexture;
        Texture lifeTexture;
        Texture ammoTexture;
        Texture bulletTexture;
        Texture aimTexture;
        Texture bombTexture;      
        Texture[] explodeEffectTextures;
        CollisionShake collisionShake;
        SpawnRules spawnRule;
        Second aliveTime;

        Texture gameOverTexture;
        Texture pressAnyButtonTexture;
        Second gameOverTime;
        readonly Second gameOverDisplayDelay = (Second)3.0f;

        Texture moveGuideTexture;
        Texture attackGuideTexture;
        Texture shakeGuideTexture;
        Texture specialGuideTexture;
        bool moveGuided;
        bool attackGuided;
        bool specialGuided;
        Second attackGuideTime;
        Second specialGuideTime;

        public Vector2 GameArea
        {
            get
            {
                return gameSystem.Viewport;
            }
        }

        enum State
        {
            Initialized,
            Running,
            Terminated
        }
        State state = State.Initialized;
    }
}
