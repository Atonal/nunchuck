﻿using OpenTK;
using System;

namespace Nunchuck.Logic.AI
{
    public class MoveToNear : AIBase
    {
        public MoveToNear(EnemyFighter owner, GameSession session, Radian angle, float nearDistance, float speed)
            : base(owner, session)
        {
            this.nearDistance = nearDistance;
            this.session = session;
            this.speed = speed;
            delta = angle.Rotate(Vector2.UnitX) * nearDistance;
            unitDelta = Vector2.Normalize(delta);
        }

        public override void Update(Second deltaTime, ref OpenTK.Vector2 velocity)
        {
            var epsilon = speed * deltaTime * 2;
            if (nearMode)
            {
                var diff = session.PlayerFighter.Position + delta - owner.Position;
                var dir = Vector2.Normalize(diff);
                velocity = dir * speed;
                if (diff.Length < epsilon)
                {
                    nearMode = false;
                }

                diff = session.PlayerFighter.Position - owner.Position;
                if (diff.Length < Math.Min(100.0f, nearDistance - epsilon))
                {
                    nearMode = false;
                }
            }
            else
            {
                var target = session.PlayerFighter;
                var diff = target.Position - owner.Position;
                var dist = diff.Length;
                if (dist > nearDistance + epsilon)
                {
                    nearMode = true;
                }
                var time = dist / speed;
                diff = target.Position + target.Velocity * time + unitDelta * dist / 2 - owner.Position;
                var dir = Vector2.Normalize(diff);
                velocity = dir * speed;
            }
        }

        bool nearMode = true;
        float speed;
        float nearDistance;
        Vector2 delta;
        Vector2 unitDelta;
    }
}
