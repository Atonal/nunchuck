﻿
namespace Nunchuck.Logic.AI
{
    public class MoveForward : MoveToNear
    {
        public MoveForward(EnemyFighter owner, GameSession session, float speed)
            : base(owner, session, Radian.Zero, 0, speed)
        { }
    }
}
