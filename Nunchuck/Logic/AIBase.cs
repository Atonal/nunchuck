﻿using OpenTK;

namespace Nunchuck.Logic
{
    public abstract class AIBase
    {
        public AIBase(EnemyFighter owner, GameSession session)
        {
            this.owner = owner;
            this.session = session;
        }

        public abstract void Update(Second deltaTime, ref Vector2 velocity);

        protected EnemyFighter owner;
        protected GameSession session;
    }
}
