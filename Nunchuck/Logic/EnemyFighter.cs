﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace Nunchuck.Logic
{
    public class EnemyFighter
    {
        public EnemyFighter(GameSession session, Vector2 position)
        {
            this.session = session;
            Position = position;
        }

        public void Update(Second deltaTime)
        {
            velocity = Vector2.Zero;
            foreach (var ai in aiList)
                ai.Update(deltaTime, ref velocity);
            if (velocity != Vector2.Zero)
                Rotation = (Radian)Math.Atan2(velocity.Y, velocity.X);
            Position = Position + velocity * deltaTime;
        }

        public void ApplyAI(AIBase ai)
        {
            aiList.Add(ai);
        }

        public int Hp { get; protected set; }
        Vector2 velocity;
        public Vector2 Position { get; protected set; }
        public Radian Rotation { get; protected set; }
        List<AIBase> aiList = new List<AIBase>();
        GameSession session;
    }
}
