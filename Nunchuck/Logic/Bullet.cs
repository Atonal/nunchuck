﻿using OpenTK;

namespace Nunchuck.Logic
{
    public class Bullet
    {
        public Bullet(Vector2 position, Vector2 velocity)
        {
            Position = position;
            this.velocity = velocity;
        }

        public void Update(Second deltaTime)
        {
            Position += velocity * deltaTime;
        }

        public Vector2 Position { get; private set; }
        Vector2 velocity;
    }
}
