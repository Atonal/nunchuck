﻿using OpenTK;
using OpenTK.Input;
using System;
using System.Diagnostics;

namespace Nunchuck.Logic
{
    public class InputState : IReadonlyInputState
    {
        public InputState()
        {
            TryConnectToNunchuck();
        }

        public void Update(Second deltaTime)
        {
            Displacement = Vector2.Zero;
            Attack = Special = Shaking = false;
            if (nunchuck != null)
            {
                GetInputFromNunchck();
            }
            GetInputFromKeyboard();
        }

        public void Stop()
        {
            if (nunchuck != null)
            {
                nunchuck.Dispose();
                nunchuck = null;
            }
        }

        void TryConnectToNunchuck()
        {
            try
            {
                Debug.WriteLine("TryConnectToNunchuck: called.");
                nunchuck = NunchuckInput.Open();
                nunchuck.NewInputArrived += BufferInputFromNunchuck;
                Debug.WriteLine("TryConnectToNunchuck: succeed.");
            }
            catch (NotSupportedException)
            {
                nunchuck = null;
                Debug.WriteLine("TryConnectToNunchuck: failed.");
            }
        }

        void BufferInputFromNunchuck()
        {
            if (nunchuck.ButtonC && !nunchuckCPressing)
                nunchuckCDown = true;
            if (nunchuck.ButtonZ && !nunchuckZPressing)
                nunchuckZDown = true;
            nunchuckCPressing = nunchuck.ButtonC;
            nunchuckZPressing = nunchuck.ButtonZ;
            Vector2 center = new Vector2(255, 255) / 2;
            Vector2 current = new Vector2(nunchuck.JoyX, nunchuck.JoyY);
            nunchuckDisplacement = (current - center) / 100.0f;
            nunchuckDisplacement = Vector2.Clamp(nunchuckDisplacement, new Vector2(-1, -1), new Vector2(1, 1));

            var newOrientation = new Vector3(nunchuck.AccX, nunchuck.AccY, nunchuck.AccZ) / 512 - Vector3.One;
            var changed = (nunchuckOrentation - newOrientation).Length > 0.2f;
            if (changed && nunchuckOrientationChanged)
            {
                nunchuckShaking = true;
                changed = false;
            }
            nunchuckOrientationChanged = changed;
            nunchuckOrentation = newOrientation;
        }

        void GetInputFromNunchck()
        {
            Attack = nunchuckZDown;
            Special = nunchuckCDown;
            Shaking = nunchuckShaking;
            Displacement = nunchuckDisplacement;
            nunchuckCDown = false;
            nunchuckZDown = false;
            nunchuckShaking = false;
        }

        void GetInputFromKeyboard()
        {
            KeyboardState state = Keyboard.GetState();
            bool isZDown = state.IsKeyDown(Key.Z);
            bool isCDown = state.IsKeyDown(Key.C);
            bool isSDown = state.IsKeyDown(Key.S);
            if (isZDown && !isZPressed) Attack = true;
            if (isCDown && !isCPressed) Special = true;
            if (isSDown && !isSPressed) Shaking = true;
            isZPressed = isZDown;
            isCPressed = isCDown;
            isSPressed = isSDown;

            const float keyboardDisplacement = 0.5f;
            if (state.IsKeyDown(Key.Left))
                Displacement += new Vector2(-keyboardDisplacement, 0);
            if (state.IsKeyDown(Key.Up))
                Displacement += new Vector2(0, keyboardDisplacement);
            if (state.IsKeyDown(Key.Down))
                Displacement += new Vector2(0, -keyboardDisplacement);
            if (state.IsKeyDown(Key.Right))
                Displacement += new Vector2(keyboardDisplacement, 0);
        }

        #region private states

        NunchuckInput nunchuck;
        bool nunchuckCPressing;
        bool nunchuckZPressing;
        bool nunchuckOrientationChanged;
        bool nunchuckCDown;
        bool nunchuckZDown;
        bool nunchuckShaking;
        Vector3 nunchuckOrentation;
        Vector2 nunchuckDisplacement;
        
        bool isZPressed;
        bool isCPressed;
        bool isSPressed;

        #endregion

        public Vector2 Displacement { get; private set; }
        public bool Attack { get; private set; }
        public bool Special { get; private set; }
        public bool Shaking { get; private set; }
    }
}
