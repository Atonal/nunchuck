﻿using Nunchuck.Rendering;
using OpenTK;
using System;

namespace Nunchuck.Logic
{
    public class CollisionShake
    {
        public CollisionShake()
        {
        }

        public void Update(Second deltaTime)
        {
            if (shakeTimeRemains > deltaTime)
            {
                shakeTimeRemains -= deltaTime;
                timeRemainsToNextTarget -= deltaTime;
                if (timeRemainsToNextTarget < Second.Zero)
                {
                    var r = new Vector2(rand.NextFloat(-1, 1), rand.NextFloat(-1, 1));
                    ShakeValue = r * (float)Math.Sqrt(shakeTimeRemains);
                    timeRemainsToNextTarget = (Second)rand.NextFloat(0.015f, 0.02f);
                }
            }
            else
            {
                ShakeValue = Vector2.Zero;
                shakeTimeRemains = Second.Zero;
            }
        }

        public void Shake(Second time)
        {
            shakeTimeRemains = (Second)Math.Max(time, shakeTimeRemains);
        }

        public void End()
        {
            shakeTimeRemains = Second.Zero;
        }

        public Vector2 ShakeValue { get; private set; }
        Second shakeTimeRemains = Second.Zero;
        Second timeRemainsToNextTarget;
        Random rand = new Random();
    }
}
