﻿using OpenTK;

namespace Nunchuck.Logic
{
    public interface IReadonlyInputState
    {
        Vector2 Displacement { get; }
        bool Attack { get; }
        bool Special { get; }
        bool Shaking { get; }
    }
}
