﻿using OpenTK;
using System;

namespace Nunchuck.Logic
{
    public class PlayerFighter
    {
        public PlayerFighter(GameSession session, Vector2 position)
        {
            this.session = session;
            Position = position;
            Rotation = Radian.Zero;
            Life = MaxLife;
        }

        public void Update(Second deltaTime, IReadonlyInputState input)
        {
            if (stunRemains > 0)
            {
                UpdateStun(deltaTime, input);
            }
            else
            {
                UpdateMove(deltaTime, input);
            }

            UpdateLife(deltaTime);
            UpdateAmmo(deltaTime);
            UpdateTarget(deltaTime);
            UpdateAttack(deltaTime, input);
        }

        void UpdateStun(Second deltaTime, IReadonlyInputState input)
        {
            Second stunReducePerShake = (Second)1.0f;
            if (input.Shaking)
                stunRemains -= stunReducePerShake;
            stunRemains -= deltaTime;

            if (stunRemains <= 0 && input.Shaking && input.Displacement.Length > 0.15f)
            {
                Velocity = Vector2.Normalize(input.Displacement) * maxVelocity / 2;
            }
        }

        const float deceleration = 200.0f;
        const float acceleration = 900.0f;
        const float maxVelocity = 300.0f;

        void UpdateMove(Second deltaTime, IReadonlyInputState input)
        {
            var velocity = Velocity;
            var speed = velocity.Length;
            if (speed < deceleration * deltaTime)
            {
                velocity = Vector2.Zero;
            }
            else
            {
                speed -= deceleration * deltaTime;
                velocity = Velocity.Normalized() * speed;
            }
            velocity += input.Displacement * acceleration * deltaTime;
            if (velocity.LengthSquared > maxVelocity * maxVelocity)
            {
                velocity.Normalize();
                velocity = velocity * maxVelocity;
            }
            Position += velocity * deltaTime;

            if (velocity.Length > 1)
            {
                Rotation = (Radian)(Math.Atan2(velocity.Y, velocity.X));
            }
            Velocity = velocity;
        }

        void UpdateLife(Second deltaTime)
        {
            if (Life == MaxLife) return;

            if (timeRemainsToLifeGain > deltaTime)
            {
                timeRemainsToLifeGain -= deltaTime;
            }
            else
            {
                if (timeIntervalToLifeGain > (Second)5.0f)
                    timeIntervalToLifeGain = (Second)5.0f;
                else
                    timeIntervalToLifeGain -= (Second)1.0f;
                timeIntervalToLifeGain = (Second)Math.Max(timeIntervalToLifeGain, 0.5f);
                timeRemainsToLifeGain = timeIntervalToLifeGain;
                Life++;
            }
        }

        void UpdateAmmo(Second deltaTime)
        {
            if (BulletAmmo == MaxAmmo) return;

            if (timeReaminsToAmmoGain > deltaTime)
            {
                timeReaminsToAmmoGain -= deltaTime;
            }
            else
            {
                timeReaminsToAmmoGain = AmmoRefillInterval;
                BulletAmmo = BulletAmmo + 1;
            }
        }

        void UpdateTarget(Second deltaTime)
        {
            if (timeRemainsToChangeTarget < deltaTime)
            {
                timeRemainsToChangeTarget -= deltaTime;
            }
            else
            {
                timeRemainsToChangeTarget = timeIntervalToChangeTarget;
                var minDist = float.MaxValue;
                Target = null;
                foreach (var e in session.EnemyFighters)
                {
                    var dist = (Position - e.Position).Length;
                    if (dist < minDist)
                    {
                        minDist = dist;
                        Target = e;
                    }
                }
            }
        }

        void UpdateAttack(Second deltaTime, IReadonlyInputState input)
        {
            if (IsStunned)
            {
                bulletReserved = 0;
                return;
            }

            if (input.Attack && Target != null && BulletAmmo > 0)
            {
                if (bulletReserved == 0)
                    timeRemainsToNextBullet = Second.Zero;
                bulletReserved = 5;
                targetSaved = Target;
                BulletAmmo = BulletAmmo - 1;
                if (BombAvailable == false)
                {
                    bulletUsed++;
                    if (bulletUsed > RequiredBulletForBomb)
                    {
                        BombAvailable = true;
                        bulletUsed = 0;
                    }
                }
            }

            if (input.Special && BombAvailable)
            {
                BombAvailable = false;
                for (int i = 0; i < 36; i++)
                {
                    for (int j = 0; j < specialCount; j++)
                    {
                        Radian angle = (Radian)(Radian.Pi * 2.0f / 36.0f * i);
                        angle = angle + Radian.Pi * 0.1f * j;
                        Vector2 dir = angle.Rotate(Vector2.UnitX);
                        session.AddBullet(Position, dir * (350 + 30 * j));
                    }
                }
                specialCount = Math.Min(specialCount + 1, 7);
            }

            if (bulletReserved > 0)
            {
                if (timeRemainsToNextBullet > deltaTime)
                {
                    timeRemainsToNextBullet -= deltaTime;
                }
                else
                {
                    var diff = targetSaved.Position - Position;
                    var dir = Vector2.Normalize(diff);
                    session.AddBullet(Position, dir * 500);
                    timeRemainsToNextBullet = timeRemainsToNextBullet - deltaTime + timeIntervalToNextBullet;
                    bulletReserved--;
                }
            }
        }

        public void Stun(Second stunTime)
        {
            stunRemains = (Second)Math.Max(stunRemains, stunTime);
            Velocity = Vector2.Zero;
        }

        public bool IsStunned
        {
            get
            {
                return stunRemains > Second.Zero;
            }
        }

        public void BoundPosition(Vector2 min, Vector2 max)
        {
            Position = Vector2.Clamp(Position, min, max);
        }

        public void Damage(int amount)
        {
            Life -= amount;
            if (Life < 0) Life = 0;

            timeRemainsToLifeGain = timeIntervalToLifeGain = (Second)20.0f;
        }

        public Vector2 Velocity { get; private set; }
        Second stunRemains = Second.Zero;
        public Vector2 Position { get; private set; }
        public Radian Rotation { get; private set; }
        public int BulletAmmo { get; private set; }
        public const int MaxAmmo = 5;
        static readonly Second AmmoRefillInterval = (Second)3.0;
        Second timeReaminsToAmmoGain = AmmoRefillInterval;

        public const int MaxLife = 10;
        public int Life { get; private set; }
        Second timeIntervalToLifeGain;
        Second timeRemainsToLifeGain;

        public EnemyFighter Target { get; private set; }
        static readonly Second timeIntervalToChangeTarget = (Second)0.5f;
        Second timeRemainsToChangeTarget = timeIntervalToChangeTarget;

        static readonly Second timeIntervalToNextBullet = (Second)0.05f;
        int bulletReserved = 0;
        Second timeRemainsToNextBullet = (Second)0;
        EnemyFighter targetSaved;

        const int RequiredBulletForBomb = 10;
        int bulletUsed = 0;
        int specialCount = 1;
        public bool BombAvailable { get; private set; }


        GameSession session;
    }
}
