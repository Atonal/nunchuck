﻿
using OpenTK;
using System;
namespace Nunchuck.Logic
{
    public class SpawnRules
    {
        static readonly Second firstSpawnDelay = (Second)5.0f;

        public SpawnRules(GameSession session)
        {
            this.session = session;
            timeRemainsForSpawn = firstSpawnDelay;
        }

        public void Update(Second deltaTime)
        {
            timeRemainsForSpawn -= deltaTime;
            if (timeRemainsForSpawn < Second.Zero)
            {
                stagePrograss++;
                if (stage == 1)
                {
                    if (stagePrograss == 5)
                    {
                        for (int i = 0; i < 3; i++)
                            SpawnEnemy();
                        timeRemainsForSpawn = (Second)15.0f;
                        stage = 2;
                        stagePrograss = 0;
                    }
                    else
                    {
                        SpawnEnemy();
                        timeRemainsForSpawn = (Second)5.0f;
                    }
                }
                else
                {
                    var step = stagePrograss / 5;
                    if (stagePrograss % 5 == 0)
                    {
                        for (int i = 0; i < 5 + step; i++)
                            SpawnEnemy();
                        timeRemainsForSpawn = (Second)(10.0f - step * 0.5f);
                    }
                    else
                    {
                        SpawnEnemy();
                        timeRemainsForSpawn = (Second)(4.0f - step * 0.25f);
                    }
                }
            }
        }

        void SpawnEnemy()
        {
            var speed = rand.NextFloat(75.0f, 150.0f);
            var distance = rand.Next(50, 200);
            if (rand.NextDouble() < 0.25)
                distance += rand.Next(100, 300);

            var pos = PickSpawnPointFromBoundary();
            var fighter = new EnemyFighter(session, pos);
            fighter.ApplyAI(new AI.MoveToNear(fighter, session, (Radian)rand.NextFloat(Radian.Pi * 2), distance, speed));
            session.AddEnemyFighter(fighter);
        }

        Vector2 PickSpawnPointFromBoundary()
        {
            const int top = 0;
            const int bottom = 1;
            const int left = 2;
            const int right = 3;
            float width = session.GameArea.X;
            float height = session.GameArea.Y;
            int r = rand.Next(4);
            float outDistance = 20;
            if (r == top)
                return new Vector2(rand.NextFloat(width), -outDistance);
            else if (r == bottom)
                return new Vector2(rand.NextFloat(width), height + outDistance);
            else if (r == left)
                return new Vector2(-outDistance, rand.NextFloat(height));
            else if (r == right)
                return new Vector2(width + outDistance, rand.NextFloat(height));
            else
                throw new ApplicationException("unhandled boundary");

        }

        Random rand = new Random();
        GameSession session;
        Second timeRemainsForSpawn;
        int stage = 1;
        int stagePrograss = 0;
    }
}
