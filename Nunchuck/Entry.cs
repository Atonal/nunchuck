﻿using System;

namespace Nunchuck
{
    static class Entry
    {
        static void Main()
        {
            var gameSystem = new GameSystem();
            gameSystem.Run();
        }
    }
}
