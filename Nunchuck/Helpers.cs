﻿using OpenTK;
using System.Drawing;

namespace Nunchuck
{
    public static class ColorHelper
    {
        public static Color Lerp(Color from, Color to, float toRatio)
        {
            float fromRatio = (1 - toRatio);
            byte a = (byte)(from.A * fromRatio + to.A * toRatio);
            byte r = (byte)(from.R * fromRatio + to.R * toRatio);
            byte g = (byte)(from.G * fromRatio + to.G * toRatio);
            byte b = (byte)(from.B * fromRatio + to.B * toRatio);
            return Color.FromArgb(a, r, g, b);
        }

        public static Color Multiply(Color color, float brightness)
        {
            byte a = (byte)(color.A * brightness);
            byte r = (byte)(color.R * brightness);
            byte g = (byte)(color.G * brightness);
            byte b = (byte)(color.B * brightness);
            return Color.FromArgb(a, r, g, b);
        }
    }

    public static class MatrixHelper
    {
        public static Matrix4 RotationYawPitchRoll(Vector3 rotation)
        {
            return RotationYawPitchRoll(
                (Radian)rotation.X,
                (Radian)rotation.Y,
                (Radian)rotation.Z);
        }

        public static Matrix4 RotationYawPitchRoll(Radian xRot, Radian yRot, Radian zRot)
        {
            Matrix4 x = Matrix4.CreateRotationX(xRot);
            Matrix4 y = Matrix4.CreateRotationY(yRot);
            Matrix4 z = Matrix4.CreateRotationZ(zRot);
            return z * y * x;
        }
    }
}
